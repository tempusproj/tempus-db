--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.4
-- Dumped by pg_dump version 9.2.4
-- Started on 2013-12-05 00:28:52 CET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE IF EXISTS svrtest;
--
-- TOC entry 2380 (class 1262 OID 12049)
-- Name: postgres; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE svrtest WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE svrtest OWNER TO postgres;

\connect svrtest

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2381 (class 1262 OID 12049)
-- Dependencies: 2380
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE svrtest IS 'testing svr connection database';


--
-- TOC entry 6 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2382 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 196 (class 3079 OID 11774)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2384 (class 0 OID 0)
-- Dependencies: 196
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 195 (class 3079 OID 17127)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 2385 (class 0 OID 0)
-- Dependencies: 195
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- TOC entry 197 (class 3079 OID 17136)
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- TOC entry 2386 (class 0 OID 0)
-- Dependencies: 197
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET search_path = public, pg_catalog;

--
-- TOC entry 254 (class 1255 OID 17170)
-- Name: add_input_to_dataset(text, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_input_to_dataset(input_table_name text, ds_id bigint) RETURNS boolean
    LANGUAGE plpgsql COST 10
    AS $$BEGIN
	IF EXISTS (SELECT 1 FROM dataset_input_tables
		WHERE dataset_id = ds_id 
		AND children_input_table = input_table_name)

	THEN RETURN TRUE; END IF;

	IF NOT EXISTS(SELECT 1 FROM datasets, input_tables 
		WHERE datasets.dataset_id = ds_id 
			AND input_tables.input_table_physical_name = input_table_name
			AND datasets.resolution = input_tables.resolution
			AND datasets.legal_time_deviation = input_tables.legal_time_deviation)
	THEN RAISE EXCEPTION 'Dataset with id % and input table ''%'' doesn''t exists or are incompatible!', ds_id, input_table_name;
	RETURN FALSE; END IF;

	INSERT INTO dataset_input_tables 
	VALUES(ds_id, input_table_name);

	RETURN TRUE;
END;$$;


ALTER FUNCTION public.add_input_to_dataset(input_table_name text, ds_id bigint) OWNER TO postgres;

--
-- TOC entry 255 (class 1255 OID 17171)
-- Name: add_input_to_dataset(text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_input_to_dataset(input_table text, dataset_name text, owner_user_name text) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$DECLARE ds_id bigint;
BEGIN
	ds_id := get_dataset_id($2, $3);
	IF ds_id = NULL
	THEN RAISE EXCEPTION 'Cannot get dataset id'; END IF;

	IF NOT EXISTS(SELECT 1 FROM datasets d, input_tables i
		WHERE d.dataset_id = ds_id 
			AND i.input_table_physical_name = $1
			AND d.resolution = i.resolution
			AND d.legal_time_deviation = i.legal_time_deviation)
	THEN RAISE EXCEPTION 'Dataset with id % and input table ''%'' doesn''t exists or are incompatible!', ds_id, $1;
	RETURN FALSE; END IF;

	INSERT INTO dataset_input_tables 
	VALUES(ds_id, $1);

	RETURN TRUE;
END
$_$;


ALTER FUNCTION public.add_input_to_dataset(input_table text, dataset_name text, owner_user_name text) OWNER TO postgres;

--
-- TOC entry 220 (class 1255 OID 78768)
-- Name: add_user(text, text, text, text, text, text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_user(uname text, password text, mail text, rname text DEFAULT ''::text, sname text DEFAULT ''::text, login text DEFAULT 'Login'::text, disabled boolean DEFAULT false) RETURNS bigint
    LANGUAGE plpgsql COST 1
    AS $_$
DECLARE passtohash text;
DECLARE hash text;
DECLARE uid bigint;
BEGIN

	IF (EXISTS(SELECT 1 FROM valid_users WHERE username = $1)
	OR char_length($1) = 0 OR char_length($2) = 0)
	THEN RAISE EXCEPTION 'Cannot create user!';

	ELSE
		passtohash := $1 || ':' || login || ':' || $2;
		hash := encode(digest(passtohash, 'md5'), 'hex');
		
		INSERT INTO valid_users(username, password, email, realname, surname, login, disabled) 
		VALUES(uname, hash, mail, rname, sname, login, disabled);

		SELECT user_id INTO uid FROM valid_users WHERE username = uname;

		RETURN uid;
	END IF;
	
END;$_$;


ALTER FUNCTION public.add_user(uname text, password text, mail text, rname text, sname text, login text, disabled boolean) OWNER TO postgres;

--
-- TOC entry 213 (class 1255 OID 16387)
-- Name: check_final_values_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION check_final_values_update() RETURNS trigger
    LANGUAGE plpgsql COST 1
    AS $$BEGIN

IF ((EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS 
	WHERE table_name = TG_TABLE_NAME AND column_name = 'is_final'))
AND OLD.is_final = TRUE)
THEN RAISE EXCEPTION 'Cannot change final values!'; RETURN NULL; END IF;

RETURN NEW;

END;$$;


ALTER FUNCTION public.check_final_values_update() OWNER TO postgres;

--
-- TOC entry 210 (class 1255 OID 16388)
-- Name: check_value_time(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION check_value_time() RETURNS trigger
    LANGUAGE plpgsql STABLE
    AS $$DECLARE
tvc RECORD;
devi_sec integer;
value_time_on_grid timestamp := now();
tmp integer;
BEGIN

SELECT INTO tvc        
	EXTRACT (EPOCH FROM resolution)::BIGINT AS res_sec,
	EXTRACT (EPOCH FROM legal_time_deviation)::BIGINT AS legal_dev_sec
FROM input_tables
WHERE input_table_physical_name = TG_TABLE_NAME;

devi_sec := (EXTRACT (EPOCH FROM NEW.value_time)::INTEGER % tvc.res_sec);

IF (devi_sec > tvc.legal_dev_sec
	AND ((devi_sec + tvc.legal_dev_sec) % tvc.res_sec ) 
	> tvc.legal_dev_sec) THEN
	RAISE EXCEPTION 'Value time % deviates % seconds which is more than allowed % seconds.',
		NEW.value_time, devi_sec, 	
		tvc.legal_dev_sec;
	RETURN NULL;

ELSE IF(devi_sec <= tvc.legal_dev_sec) THEN	
		value_time_on_grid := timestamp 'epoch' + 
		(((EXTRACT(epoch FROM NEW.value_time)::INTEGER - devi_sec) || ' seconds')::INTERVAL);
	
	ELSE
		tmp := (EXTRACT(epoch FROM NEW.value_time)::INTEGER + tvc.legal_dev_sec) % tvc.res_sec;
		value_time_on_grid := timestamp 'epoch' +
		((
		(EXTRACT(epoch FROM NEW.value_time)::INTEGER + tvc.legal_dev_sec - tmp) 
		|| ' seconds')::INTERVAL);
	END IF;
	NEW.value_time := value_time_on_grid;
	RETURN NEW;
END IF;
END;$$;


ALTER FUNCTION public.check_value_time() OWNER TO postgres;

--
-- TOC entry 256 (class 1255 OID 17173)
-- Name: constraint_key_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION constraint_key_update() RETURNS trigger
    LANGUAGE plpgsql IMMUTABLE COST 1
    AS $$BEGIN
IF NEW.value_time != OLD.value_time
THEN RAISE 'Cannot change key (value_time)=(%)', OLD.value_time; RETURN NULL;
ELSE RETURN NEW; END IF;
END;$$;


ALTER FUNCTION public.constraint_key_update() OWNER TO postgres;

--
-- TOC entry 212 (class 1255 OID 17174)
-- Name: create_dataset(bigint, text, timestamp without time zone, timestamp without time zone, timestamp without time zone, interval, interval, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION create_dataset(u_id bigint, ds_name text, first_time timestamp without time zone DEFAULT ((0)::abstime)::timestamp without time zone, last_update_time timestamp without time zone DEFAULT ((0)::abstime)::timestamp without time zone, last_decon_time timestamp without time zone DEFAULT ((0)::abstime)::timestamp without time zone, resolution interval DEFAULT '00:00:01'::interval, legal_time_deviation interval DEFAULT '00:00:00'::interval, decon_levels_count integer DEFAULT 1, description text DEFAULT 'No description'::text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
ds_id bigint;
BEGIN

	IF NOT EXISTS(SELECT 1 FROM valid_users WHERE valid_users.user_id = u_id) 
	THEN RAISE EXCEPTION 'The user with user id % does not exist!', u_id; RETURN FALSE;
	END IF;
	
	IF EXISTS(SELECT 1 FROM datasets 
		WHERE dataset_name = ds_name AND owner_user_id = u_id)
	THEN 
		RAISE EXCEPTION 'Cannot create dataset! The same dataset already exists for the same user!'; 
		RETURN FALSE;
	END IF;

	SELECT nextval('datasets_dataset_id_seq') INTO ds_id;

	INSERT INTO datasets
		VALUES(first_time, last_decon_time,
		resolution, legal_time_deviation, 
		description, ds_name,
		ds_id, u_id, decon_levels_count);	
	RETURN TRUE;
END;$$;


ALTER FUNCTION public.create_dataset(u_id bigint, ds_name text, first_time timestamp without time zone, last_update_time timestamp without time zone, last_decon_time timestamp without time zone, resolution interval, legal_time_deviation interval, decon_levels_count integer, description text) OWNER TO postgres;

--
-- TOC entry 258 (class 1255 OID 17175)
-- Name: create_dataset(text, text, timestamp without time zone, timestamp without time zone, timestamp without time zone, interval, interval, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION create_dataset(user_name text, ds_name text, first_time timestamp without time zone DEFAULT ((0)::abstime)::timestamp without time zone, last_update_time timestamp without time zone DEFAULT ((0)::abstime)::timestamp without time zone, last_decon_time timestamp without time zone DEFAULT ((0)::abstime)::timestamp without time zone, resolution interval DEFAULT '00:00:01'::interval, legal_time_deviation interval DEFAULT '00:00:00'::interval, decon_levels_count integer DEFAULT 1, description text DEFAULT 'No description'::text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
ds_id bigint;
uid bigint;
BEGIN

	IF EXISTS(SELECT 1 FROM valid_users WHERE valid_users.username = user_name)
	THEN
		SELECT user_id INTO uid FROM valid_users
		WHERE valid_users.username = user_name;
	ELSE
		RAISE EXCEPTION 'The user with username % does not exist!', user_name; RETURN FALSE;
	END IF;

	IF EXISTS(SELECT 1 FROM datasets 
		WHERE dataset_name = ds_name AND owner_user_id = uid)
	THEN 
		RAISE EXCEPTION 'Cannot create dataset! The same dataset already exists for the same user!'; 
		RETURN FALSE;
	END IF;

	SELECT nextval('datasets_dataset_id_seq') INTO ds_id;

	INSERT INTO datasets
		VALUES(first_time, last_decon_time,
		resolution, legal_time_deviation,
		description, ds_name,
		ds_id, uid, decon_levels_count);	
	RETURN TRUE;
END;$$;


ALTER FUNCTION public.create_dataset(user_name text, ds_name text, first_time timestamp without time zone, last_update_time timestamp without time zone, last_decon_time timestamp without time zone, resolution interval, legal_time_deviation interval, decon_levels_count integer, description text) OWNER TO postgres;

--
-- TOC entry 291 (class 1255 OID 17784)
-- Name: create_decon_table(text, bigint, text, text, smallint, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION create_decon_table(user_name text, dataset_id bigint, queue_table_name text, queue_column text, swt_level smallint, wavelet text) RETURNS text
    LANGUAGE plpgsql COST 10
    AS $_$DECLARE unq_name text := '';
	qry text;
	level_cnt smallint := 0;
BEGIN

	IF NOT EXISTS(SELECT 1 FROM datasets, valid_users AS vu WHERE datasets.dataset_id = $2 
		AND vu.username = $1 AND vu.user_id = datasets.owner_user_id) 	
	OR NOT EXISTS(SELECT 1 FROM information_schema.columns as isc
		WHERE isc.table_name = $3 AND isc.column_name = $4)
	THEN RAISE EXCEPTION 'The given arguments are invalid to further process the request.'; 
	RETURN ''; END IF;

	IF NOT EXISTS(SELECT 1 FROM dataset_input_tables as dit WHERE dit.dataset_id = $2 
		AND children_input_table = $3) THEN
		RAISE EXCEPTION 'The given input table % doesn''t belong to the specified dataset with id %.', $3, $2;
		RETURN ''; END IF;

	BEGIN
		SELECT literal_decon_table INTO STRICT unq_name FROM input_decons 
		WHERE literal_input_table = queue_table_name	
		AND source_column = queue_column
		AND input_decons.dataset_id = $2
		AND input_decons.swt_level = $5;

		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			unq_name := 'd_' || nextval('decon_table_seq');
			qry := 'CREATE TABLE ' || unq_name || 
			'( value_time timestamp without time zone not null, ';		
			LOOP
				EXIT WHEN level_cnt > swt_level;
				qry := qry || 'level' || level_cnt || ' double precision default 0, ';
				level_cnt := level_cnt + 1;

			END LOOP;

			qry := qry || 'update_time timestamp without time zone default date_trunc(''seconds''::text, now()), 
				CONSTRAINT ' || unq_name || '_pk PRIMARY KEY(value_time))
				WITH (OIDS = FALSE);	
				CREATE TRIGGER on_value_update
				AFTER UPDATE
				ON ' || unq_name || '
				FOR EACH ROW
				EXECUTE PROCEDURE stamp_update();';

			EXECUTE qry;	

			INSERT INTO input_decons VALUES(queue_table_name, queue_column, wavelet, unq_name, dataset_id, swt_level);
			RETURN unq_name;
		WHEN TOO_MANY_ROWS THEN
			RAISE EXCEPTION 'Two or more Decon tables exist in database with name %! Critical failure!', unq_name;
			RETURN unq_name;
	END;
	RETURN unq_name;
END;$_$;


ALTER FUNCTION public.create_decon_table(user_name text, dataset_id bigint, queue_table_name text, queue_column text, swt_level smallint, wavelet text) OWNER TO postgres;

--
-- TOC entry 214 (class 1255 OID 53723)
-- Name: default_timezone(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION default_timezone() RETURNS text
    LANGUAGE sql IMMUTABLE COST 1
    AS $$select abbrev from pg_timezone_names where name = 'EET'$$;


ALTER FUNCTION public.default_timezone() OWNER TO postgres;

--
-- TOC entry 289 (class 1255 OID 78769)
-- Name: create_input_table(text, text, text[], interval, interval, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION create_input_table(owner_user_name text, input_table_name text, values_columns text[], resolution interval DEFAULT '00:00:01'::interval, legal_time_deviation interval DEFAULT '00:00:00'::interval, timezone text DEFAULT default_timezone(), description text DEFAULT 'Unnamed queue'::text) RETURNS text
    LANGUAGE plpgsql COST 1
    AS $$
DECLARE q text;
DECLARE cnt integer := 1;
DECLARE uniq_tab_name text;
BEGIN

uniq_tab_name := quote_ident(owner_user_name) || '_' || nextval('input_table_name_unique_part')::text;

q := 'CREATE TABLE ' || uniq_tab_name || '( '; 

LOOP
	EXIT WHEN cnt > array_length(values_columns, 1);
	q := q || quote_ident(values_columns[cnt]) || ' double precision default 0, ';
	cnt := cnt + 1;
END LOOP;

q := q || ' CONSTRAINT ' || uniq_tab_name || '_pk PRIMARY KEY (value_time))
	INHERITS (template_table_input_values)
	WITH (
	 OIDS=FALSE
	);';


q := q || 'CREATE TRIGGER on_value_update
	AFTER UPDATE
	ON ' || uniq_tab_name || '
	FOR EACH ROW
	EXECUTE PROCEDURE stamp_update();';

q := q || 'CREATE TRIGGER update_logger
	AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
	ON ' || uniq_tab_name || '
	FOR EACH STATEMENT
	EXECUTE PROCEDURE log_queue_updates();';

q := q || 'CREATE TRIGGER time_grid_check
	BEFORE INSERT OR UPDATE
	ON ' || uniq_tab_name || '
	FOR EACH ROW
	EXECUTE PROCEDURE row_on_time_grid();';

q := q || 'CREATE TRIGGER on_final_value_update
	BEFORE UPDATE
	ON ' || uniq_tab_name || '
	FOR EACH ROW
	EXECUTE PROCEDURE  check_final_values_update();';

q := q || 'CREATE TRIGGER forbid_key_update
	BEFORE UPDATE
	ON ' || uniq_tab_name || '
	FOR EACH ROW
	EXECUTE PROCEDURE constraint_key_update();';
  

EXECUTE q;

INSERT INTO input_tables VALUES(uniq_tab_name, resolution, legal_time_deviation, 
	description, input_table_name, owner_user_name, timezone);

RETURN uniq_tab_name;
END;$$;


ALTER FUNCTION public.create_input_table(owner_user_name text, input_table_name text, values_columns text[], resolution interval, legal_time_deviation interval, timezone text, description text) OWNER TO postgres;

--
-- TOC entry 259 (class 1255 OID 17177)
-- Name: dataset_exists(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dataset_exists(bigint) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE COST 1
    AS $_$BEGIN
RETURN EXISTS(SELECT 1 FROM datasets WHERE dataset_id = $1);
END;$_$;


ALTER FUNCTION public.dataset_exists(bigint) OWNER TO postgres;

--
-- TOC entry 260 (class 1255 OID 17178)
-- Name: dataset_exists(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dataset_exists(user_name text, ds_name text) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE COST 1
    AS $$DECLARE
uid BIGINT;
BEGIN
	SELECT user_id INTO uid FROM valid_users WHERE username = user_name;	
	RETURN EXISTS(SELECT 1 FROM datasets WHERE dataset_name = ds_name 
		AND owner_user_id = uid);
END;$$;


ALTER FUNCTION public.dataset_exists(user_name text, ds_name text) OWNER TO postgres;

--
-- TOC entry 261 (class 1255 OID 17179)
-- Name: delete_dataset(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION delete_dataset(ds_id bigint) RETURNS boolean
    LANGUAGE plpgsql COST 1
    AS $$BEGIN

	DELETE FROM datasets WHERE dataset_id = ds_id;

	DELETE FROM dataset_input_tables WHERE dataset_id = ds_id;	

	RETURN TRUE;
END;$$;


ALTER FUNCTION public.delete_dataset(ds_id bigint) OWNER TO postgres;

--
-- TOC entry 262 (class 1255 OID 17180)
-- Name: delete_dataset(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION delete_dataset(user_name text, dataset_name text) RETURNS boolean
    LANGUAGE plpgsql COST 1
    AS $_$DECLARE ds_id bigint;
BEGIN
	SELECT dataset_id INTO ds_id FROM datasets WHERE 
		owner_user_name = $1 AND dataset_name = $2;
	
	RETURN delete_dataset(ds_id);
END;$_$;


ALTER FUNCTION public.delete_dataset(user_name text, dataset_name text) OWNER TO postgres;

--
-- TOC entry 263 (class 1255 OID 17181)
-- Name: delete_input_table(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION delete_input_table(physical_table_name text, owner_user text) RETURNS boolean
    LANGUAGE plpgsql COST 10
    AS $_$BEGIN

	IF NOT EXISTS(SELECT 1 FROM input_tables 
		WHERE input_table_physical_name = $1
		AND owner_user_name = $2)
	THEN RAISE EXCEPTION 'Input table % doesn''t exist or no permissions to delete it!', $1;	
	RETURN FALSE;
	END IF;
	
	DELETE FROM input_tables WHERE input_table_physical_name = $1 
	AND owner_user_name = $2;

	DELETE FROM dataset_input_tables WHERE children_input_table = $1;
	DELETE FROM update_log WHERE input_table = $1;
	EXECUTE 'DROP TABLE ' || $1;
	
	RETURN TRUE;
END;$_$;


ALTER FUNCTION public.delete_input_table(physical_table_name text, owner_user text) OWNER TO postgres;

--
-- TOC entry 264 (class 1255 OID 17182)
-- Name: delete_user(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION delete_user(user_name text) RETURNS boolean
    LANGUAGE plpgsql COST 1
    AS $_$BEGIN	
	IF NOT EXISTS(SELECT 1 FROM valid_users WHERE username = $1) THEN RETURN FALSE; END IF;
	DELETE FROM valid_users WHERE username = $1;
	RETURN TRUE;
END;$_$;


ALTER FUNCTION public.delete_user(user_name text) OWNER TO postgres;

--
-- TOC entry 265 (class 1255 OID 17183)
-- Name: delete_user(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION delete_user(uid bigint) RETURNS boolean
    LANGUAGE plpgsql COST 1
    AS $_$BEGIN
	IF NOT EXISTS(SELECT 1 FROM valid_users WHERE user_id = $1) THEN RETURN FALSE; END IF;
	DELETE FROM valid_users WHERE user_id = $1;
	RETURN TRUE;
END;$_$;


ALTER FUNCTION public.delete_user(uid bigint) OWNER TO postgres;

--
-- TOC entry 266 (class 1255 OID 17184)
-- Name: divide_into_intervals(timestamp without time zone, timestamp without time zone, interval); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION divide_into_intervals(_from timestamp without time zone, _to timestamp without time zone, intrvl interval) RETURNS SETOF timestamp without time zone
    LANGUAGE plpgsql
    AS $$declare t timestamp without time zone;
tmp timestamp without time zone;
begin
tmp := _from;
while tmp <= _to loop
t := tmp;
tmp := tmp + intrvl;
return next t;
end 
loop;
return; 
end;
$$;


ALTER FUNCTION public.divide_into_intervals(_from timestamp without time zone, _to timestamp without time zone, intrvl interval) OWNER TO postgres;

--
-- TOC entry 267 (class 1255 OID 17185)
-- Name: extract_volume_from_timestamp(timestamp without time zone); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION extract_volume_from_timestamp("time" timestamp without time zone) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE COST 1
    AS $$BEGIN
	RETURN ((EXTRACT(S FROM time) - EXTRACT(S FROM time)::integer) * 1000000)::integer;
END;$$;


ALTER FUNCTION public.extract_volume_from_timestamp("time" timestamp without time zone) OWNER TO postgres;

--
-- TOC entry 268 (class 1255 OID 17186)
-- Name: generate_decon_values_table_name(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION generate_decon_values_table_name() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin NEW.decon_values_table_name = NEW.username || 
'_' || NEW.dataset || '_decon_values'; return new; end $$;


ALTER FUNCTION public.generate_decon_values_table_name() OWNER TO postgres;

--
-- TOC entry 269 (class 1255 OID 17187)
-- Name: generate_input_values_table_name(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION generate_input_values_table_name() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin NEW.input_values_table_name = NEW.username || 
'_' || NEW.dataset || '_input_values'; return new; end $$;


ALTER FUNCTION public.generate_input_values_table_name() OWNER TO postgres;

--
-- TOC entry 270 (class 1255 OID 17188)
-- Name: generate_requests_table_name(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION generate_requests_table_name() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ begin NEW.user_request_table_name = NEW.username || 
'_' || NEW.dataset || '_requests'; return new; end $$;


ALTER FUNCTION public.generate_requests_table_name() OWNER TO postgres;

--
-- TOC entry 271 (class 1255 OID 17189)
-- Name: get_all_table_columns(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_all_table_columns(tn text) RETURNS SETOF text
    LANGUAGE plpgsql IMMUTABLE COST 10 ROWS 5
    AS $_$BEGIN
	RETURN QUERY SELECT column_name::text FROM information_schema.columns WHERE table_name = $1 ORDER BY ordinal_position ASC;
END;$_$;


ALTER FUNCTION public.get_all_table_columns(tn text) OWNER TO postgres;

--
-- TOC entry 272 (class 1255 OID 17190)
-- Name: get_dataset_id(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_dataset_id(dataset_name text, owner_user_name text) RETURNS bigint
    LANGUAGE plpgsql IMMUTABLE COST 10
    AS $_$DECLARE ds_id bigint;
BEGIN
	SELECT dataset_id INTO ds_id FROM datasets d, valid_users v WHERE d.dataset_name = $1 
	AND v.username = $2 AND d.owner_user_id = v.user_id;

	RETURN ds_id;	
END;$_$;


ALTER FUNCTION public.get_dataset_id(dataset_name text, owner_user_name text) OWNER TO postgres;

--
-- TOC entry 273 (class 1255 OID 17191)
-- Name: get_dataset_input_tables(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_dataset_input_tables(ds_id bigint) RETURNS SETOF text
    LANGUAGE sql IMMUTABLE COST 1
    AS $_$SELECT children_input_table FROM dataset_input_tables WHERE dataset_id = $1$_$;


ALTER FUNCTION public.get_dataset_input_tables(ds_id bigint) OWNER TO postgres;

--
-- TOC entry 274 (class 1255 OID 17192)
-- Name: get_dataset_input_tables(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_dataset_input_tables(text, text) RETURNS SETOF text
    LANGUAGE plpgsql IMMUTABLE COST 10
    AS $_$DECLARE
	ds_id bigint;
	uid bigint;
BEGIN

	SELECT user_id INTO uid FROM valid_users WHERE username = $2;
	SELECT dataset_id INTO ds_id FROM datasets WHERE dataset_name = $1
		AND owner_user_id = uid;

	RETURN QUERY SELECT children_input_table
		FROM dataset_input_tables	WHERE dataset_id = ds_id;

END;$_$;


ALTER FUNCTION public.get_dataset_input_tables(text, text) OWNER TO postgres;

--
-- TOC entry 275 (class 1255 OID 17193)
-- Name: get_dataset_interval(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_dataset_interval(ds_name text, uname text) RETURNS SETOF text
    LANGUAGE plpgsql IMMUTABLE COST 1
    AS $_$BEGIN
RETURN QUERY SELECT resolution::text FROM datasets, valid_users 
WHERE dataset_name = $1 AND username = $2 AND owner_user_id = user_id;
END;$_$;


ALTER FUNCTION public.get_dataset_interval(ds_name text, uname text) OWNER TO postgres;

--
-- TOC entry 257 (class 1255 OID 17194)
-- Name: get_input_table_interval(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_input_table_interval(it_name text, uname text) RETURNS text
    LANGUAGE plpgsql IMMUTABLE COST 1
    AS $$DECLARE res text;
BEGIN

	IF NOT is_owner_of_input(uname, it_name)
		THEN RAISE EXCEPTION 'Input table doesn''t exist or no permissions for metadata retrieval.';
		RETURN ''; 
	END IF;

	SELECT resolution::text INTO res FROM input_tables WHERE input_table_physical_name = it_name;
	RETURN res;
END;$$;


ALTER FUNCTION public.get_input_table_interval(it_name text, uname text) OWNER TO postgres;

--
-- TOC entry 211 (class 1255 OID 42376)
-- Name: get_non_value_columns(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_non_value_columns(table_name text) RETURNS SETOF text
    LANGUAGE sql IMMUTABLE COST 1
    AS $$SELECT get_all_table_columns(table_name) EXCEPT (SELECT get_value_columns(table_name));
$$;


ALTER FUNCTION public.get_non_value_columns(table_name text) OWNER TO postgres;

--
-- TOC entry 276 (class 1255 OID 17196)
-- Name: get_user_datasets(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_user_datasets(user_name text) RETURNS SETOF bigint
    LANGUAGE plpgsql IMMUTABLE ROWS 100
    AS $_$DECLARE uid bigint;
BEGIN	
	RETURN QUERY SELECT dataset_id FROM datasets d, valid_users v WHERE v.username = $1
	AND d.owner_user_id = v.user_id;
END;$_$;


ALTER FUNCTION public.get_user_datasets(user_name text) OWNER TO postgres;

--
-- TOC entry 277 (class 1255 OID 17197)
-- Name: get_user_datasets(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_user_datasets(user_id bigint) RETURNS SETOF bigint
    LANGUAGE plpgsql IMMUTABLE COST 1 ROWS 10
    AS $_$
BEGIN
	RETURN QUERY SELECT d.dataset_id FROM datasets d WHERE d.owner_user_id = $1;
END;$_$;


ALTER FUNCTION public.get_user_datasets(user_id bigint) OWNER TO postgres;

--
-- TOC entry 278 (class 1255 OID 17198)
-- Name: get_user_queues_logical_names(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_user_queues_logical_names(username text) RETURNS SETOF text
    LANGUAGE plpgsql IMMUTABLE COST 10 ROWS 10
    AS $_$BEGIN
	RETURN QUERY SELECT input_logical_name FROM input_tables WHERE owner_user_name = $1;
END;$_$;


ALTER FUNCTION public.get_user_queues_logical_names(username text) OWNER TO postgres;

--
-- TOC entry 279 (class 1255 OID 17199)
-- Name: get_user_queues_physical_names(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_user_queues_physical_names(username text) RETURNS SETOF text
    LANGUAGE plpgsql IMMUTABLE COST 10 ROWS 10
    AS $_$BEGIN
	RETURN QUERY SELECT input_table_physical_name FROM input_tables WHERE owner_user_name = $1;
END;$_$;


ALTER FUNCTION public.get_user_queues_physical_names(username text) OWNER TO postgres;

--
-- TOC entry 290 (class 1255 OID 17195)
-- Name: get_value_columns(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_value_columns(tn text) RETURNS SETOF text
    LANGUAGE plpgsql IMMUTABLE COST 10 ROWS 5
    AS $_$BEGIN
	RETURN QUERY SELECT column_name::text FROM 
	(SELECT column_name, ordinal_position FROM information_schema.columns WHERE table_name = $1 
		EXCEPT (SELECT column_name, ordinal_position FROM information_schema.columns WHERE table_name='template_table_input_values')
	) AS columns ORDER BY ordinal_position ASC;	
END;$_$;


ALTER FUNCTION public.get_value_columns(tn text) OWNER TO postgres;

--
-- TOC entry 280 (class 1255 OID 17201)
-- Name: input_table_exists(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION input_table_exists(physical_name text) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE COST 1
    AS $$BEGIN
	RETURN EXISTS(SELECT 1 FROM information_schema.tables WHERE table_name = physical_name)
	AND EXISTS(SELECT 1 FROM input_tables WHERE input_table_physical_name = physical_name);	
END;$$;


ALTER FUNCTION public.input_table_exists(physical_name text) OWNER TO postgres;

--
-- TOC entry 281 (class 1255 OID 17202)
-- Name: input_table_exists(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION input_table_exists(logical_name text, user_name text) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE COST 1
    AS $_$BEGIN
	RETURN EXISTS(SELECT 1 FROM input_tables WHERE input_logical_name = $1 AND owner_user_name = $2); 
END;$_$;


ALTER FUNCTION public.input_table_exists(logical_name text, user_name text) OWNER TO postgres;

--
-- TOC entry 282 (class 1255 OID 17203)
-- Name: is_owner_of_dataset(text, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION is_owner_of_dataset(uname text, ds_id bigint) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE COST 1
    AS $_$BEGIN
	RETURN EXISTS (SELECT 1 FROM datasets d, valid_users v 
	WHERE v.username = $1 AND d.dataset_id = $2 AND d.owner_user_id = v.user_id);
	
END;$_$;


ALTER FUNCTION public.is_owner_of_dataset(uname text, ds_id bigint) OWNER TO postgres;

--
-- TOC entry 283 (class 1255 OID 17204)
-- Name: is_owner_of_input(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION is_owner_of_input(uname text, it text) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE COST 1
    AS $$DECLARE uid bigint;
BEGIN
	RETURN EXISTS(SELECT 1 FROM input_tables WHERE owner_user_name = uname AND input_table_physical_name = it);	
END;$$;


ALTER FUNCTION public.is_owner_of_input(uname text, it text) OWNER TO postgres;

--
-- TOC entry 218 (class 1255 OID 53746)
-- Name: is_timezone(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION is_timezone(tz text) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE LEAKPROOF COST 1
    AS $_$BEGIN 
IF	EXISTS(SELECT 1 FROM pg_timezone_names WHERE abbrev = $1 LIMIT 1) 
	OR 
	EXISTS(SELECT 1 FROM pg_timezone_names WHERE name = $1 LIMIT 1)
THEN RETURN TRUE;
ELSE RETURN FALSE;
END IF;
END;$_$;


ALTER FUNCTION public.is_timezone(tz text) OWNER TO postgres;

--
-- TOC entry 284 (class 1255 OID 17205)
-- Name: liberate_user_data(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION liberate_user_data(uname text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$DECLARE uid bigint;
BEGIN


	IF NOT EXISTS (SELECT 1 FROM valid_users WHERE username = uname) THEN RETURN FALSE; END IF;
	
	SELECT user_id INTO uid FROM valid_users WHERE username = uname;

	
--	FOR dataset IN PERFORM get_user_datasets(uid) LOOP
	
--		FOR input_que IN PERFORM get_dataset_input_tables(dataset) LOOP
--			PERFORM delete_input_table(input_que, uname);
--		END LOOP;

--		delete_dataset(dataset);
--	END LOOP;	

--	delete_user(uid);

	RETURN TRUE;	
END;$$;


ALTER FUNCTION public.liberate_user_data(uname text) OWNER TO postgres;

--
-- TOC entry 215 (class 1255 OID 16421)
-- Name: log_queue_updates(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION log_queue_updates() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF EXISTS (SELECT 1 FROM update_log 
		WHERE input_table = TG_TABLE_NAME)
	THEN	
		UPDATE update_log 
		SET last_update = current_timestamp		
		WHERE input_table = TG_TABLE_NAME;
	ELSE 
		INSERT INTO update_log(last_update, input_table) 
		VALUES(current_timestamp, TG_TABLE_NAME);
	END IF;

RETURN NULL;
END 


$$;


ALTER FUNCTION public.log_queue_updates() OWNER TO postgres;

--
-- TOC entry 285 (class 1255 OID 17206)
-- Name: make_decon_table_name(text, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION make_decon_table_name(base_table_name text, level integer, sub_level integer DEFAULT (-1)) RETURNS text
    LANGUAGE plpgsql IMMUTABLE COST 1
    AS $$
DECLARE ret text;
BEGIN

ret := base_table_name || '_' || level::text;

IF sub_level >= 0 THEN
	ret := ret || '_' || sub_level::text;
END IF;

RETURN ret;
END;$$;


ALTER FUNCTION public.make_decon_table_name(base_table_name text, level integer, sub_level integer) OWNER TO postgres;

--
-- TOC entry 286 (class 1255 OID 17207)
-- Name: make_input_table_name(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION make_input_table_name(desired_name text DEFAULT 'unnamed_input'::text) RETURNS text
    LANGUAGE plpgsql IMMUTABLE COST 1
    AS $$DECLARE cnt integer := 0;
DECLARE gen text;
BEGIN

	IF NOT EXISTS(SELECT 1 FROM information_schema.tables 
		WHERE table_name = desired_name)
	THEN RETURN desired_name;

	ELSE		
		LOOP 
			gen := desired_name || '_' || cnt;
			IF NOT EXISTS(SELECT 1 FROM information_schema.tables
				WHERE table_name = gen)
			THEN RETURN gen;
			ELSE cnt := cnt + 1;
			END IF;
		END LOOP;
	END IF;

END;$$;


ALTER FUNCTION public.make_input_table_name(desired_name text) OWNER TO postgres;

--
-- TOC entry 287 (class 1255 OID 17208)
-- Name: remove_input_from_dataset(text, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION remove_input_from_dataset(input_table text, ds_id bigint) RETURNS boolean
    LANGUAGE plpgsql COST 1
    AS $_$BEGIN
	DELETE FROM dataset_input_tables WHERE dataset_id = $2 AND children_input_table = $1;
	RETURN TRUE;
END;$_$;


ALTER FUNCTION public.remove_input_from_dataset(input_table text, ds_id bigint) OWNER TO postgres;

--
-- TOC entry 219 (class 1255 OID 17209)
-- Name: row_on_time_grid(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION row_on_time_grid() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
tvc RECORD;
devi_sec integer;
value_time_on_grid timestamp;
user_tz_value_time timestamp;
tmp integer;
seconds_since_epoch bigint;
tz text;
BEGIN
SET LOCAL TIMEZONE TO 'UTC';
SELECT INTO tvc        
	EXTRACT (EPOCH FROM resolution)::BIGINT AS res_sec,
	EXTRACT (EPOCH FROM legal_time_deviation)::BIGINT AS legal_dev_sec,
	timezone as tz
FROM input_tables
WHERE input_table_physical_name = TG_TABLE_NAME;

RAISE NOTICE 'UTC value_time: %, Resolution: % seconds, LTD: % seconds, current selected timezone: %', NEW.value_time, tvc.res_sec, tvc.legal_dev_sec, tvc.tz;

user_tz_value_time := (NEW.value_time AT TIME ZONE 'UTC' AT TIME ZONE tvc.tz)::timestamp;
RAISE NOTICE 'Timezoned value_time: %', user_tz_value_time;


seconds_since_epoch := EXTRACT (EPOCH FROM user_tz_value_time)::INTEGER;


RAISE NOTICE 'Seconds since timezoned epoch: %', seconds_since_epoch;

devi_sec := (seconds_since_epoch % tvc.res_sec);
RAISE NOTICE 'deviation in seconds: %',  devi_sec;

IF (devi_sec > tvc.legal_dev_sec
	AND ((devi_sec + tvc.legal_dev_sec) % tvc.res_sec ) > tvc.legal_dev_sec) 
	THEN RAISE EXCEPTION 'Value time % deviates % seconds on interval % which is more than allowed % seconds.',
		NEW.value_time, devi_sec, tvc.res_sec, tvc.legal_dev_sec;
	RETURN NULL;

ELSE IF(devi_sec <= tvc.legal_dev_sec) THEN	
		value_time_on_grid := timestamp 'epoch' + 
		(((seconds_since_epoch - devi_sec) || ' seconds')::INTERVAL);
	
	ELSE
		tmp := (seconds_since_epoch + tvc.legal_dev_sec) % tvc.res_sec;
		value_time_on_grid := timestamp 'epoch' +
		(((seconds_since_epoch + tvc.legal_dev_sec - tmp) || ' seconds')::INTERVAL);
	END IF;	
	value_time_on_grid := value_time_on_grid AT TIME ZONE tvc.tz AT TIME ZONE 'UTC';
END IF;
RAISE NOTICE 'new value time: %', value_time_on_grid;
NEW.value_time := (value_time_on_grid)::timestamp;
RETURN NEW;
END;$$;


ALTER FUNCTION public.row_on_time_grid() OWNER TO postgres;

--
-- TOC entry 288 (class 1255 OID 17210)
-- Name: soci_test(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION soci_test(msg character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$ begin   return msg; end $$;


ALTER FUNCTION public.soci_test(msg character varying) OWNER TO postgres;

--
-- TOC entry 217 (class 1255 OID 16426)
-- Name: stamp_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION stamp_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN 
NEW.update_time = date_trunc('seconds', CURRENT_TIMESTAMP); RETURN NEW; 

END;$$;


ALTER FUNCTION public.stamp_update() OWNER TO postgres;

--
-- TOC entry 216 (class 1255 OID 53725)
-- Name: userid_to_username(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION userid_to_username(userid bigint) RETURNS text
    LANGUAGE sql IMMUTABLE COST 10
    AS $_$select username from valid_users where user_id = $1;$_$;


ALTER FUNCTION public.userid_to_username(userid bigint) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 169 (class 1259 OID 17211)
-- Name: dataset_input_tables; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dataset_input_tables (
    dataset_id bigint NOT NULL,
    children_input_table text NOT NULL,
    perm bit varying(4) DEFAULT B'1110'::"bit" NOT NULL
);


ALTER TABLE public.dataset_input_tables OWNER TO postgres;

--
-- TOC entry 2387 (class 0 OID 0)
-- Dependencies: 169
-- Name: TABLE dataset_input_tables; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE dataset_input_tables IS 'PERMISSION is flags array representing the read/write permissions on the owner and on the public.
First two bits (bit[0] and bit[1]) are for the owner read/write permissions.
Second two bits (bit[2] and bit[3]) are for the public read/write permissions.

Default is bit[0] = 1; bit[1] = 1; bit[2] = 1; bit[3] = 0; i.e owner and public can read but only owner can modify the data.';


--
-- TOC entry 171 (class 1259 OID 17224)
-- Name: datasets; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE datasets (
    first_time timestamp without time zone DEFAULT ((0)::abstime)::timestamp without time zone,
    last_decon_time timestamp without time zone DEFAULT ((0)::abstime)::timestamp without time zone,
    resolution interval DEFAULT '00:00:01'::interval NOT NULL,
    legal_time_deviation interval DEFAULT '00:00:00'::interval NOT NULL,
    description text DEFAULT 'No description'::text NOT NULL,
    dataset_name text NOT NULL,
    dataset_id bigint NOT NULL,
    owner_user_id bigint NOT NULL,
    decon_levels_count smallint DEFAULT 2 NOT NULL,
    priority integer DEFAULT 0,
    CONSTRAINT deviation_smaller_than_resolution CHECK ((legal_time_deviation < resolution)),
    CONSTRAINT valid_dataset_name CHECK ((dataset_name ~* '^[a-z][a-z0-9_]{3,30}$'::text))
);


ALTER TABLE public.datasets OWNER TO postgres;

--
-- TOC entry 2388 (class 0 OID 0)
-- Dependencies: 171
-- Name: TABLE datasets; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE datasets IS 'Metadata for the existig datasets.';


--
-- TOC entry 172 (class 1259 OID 17239)
-- Name: datasets_dataset_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE datasets_dataset_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.datasets_dataset_id_seq OWNER TO postgres;

--
-- TOC entry 2389 (class 0 OID 0)
-- Dependencies: 172
-- Name: datasets_dataset_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE datasets_dataset_id_seq OWNED BY datasets.dataset_id;


--
-- TOC entry 182 (class 1259 OID 17809)
-- Name: decon_table_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE decon_table_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.decon_table_seq OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 17680)
-- Name: input_decons; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE input_decons (
    literal_input_table text NOT NULL,
    source_column text NOT NULL,
    wavelet_used text NOT NULL,
    literal_decon_table text NOT NULL,
    swt_level smallint NOT NULL,
    time_of_decon timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.input_decons OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 17241)
-- Name: input_table_name_unique_part; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE input_table_name_unique_part
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.input_table_name_unique_part OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 17243)
-- Name: input_tables; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE input_tables (
    input_table_physical_name text NOT NULL,
    resolution interval DEFAULT '00:01:00'::interval NOT NULL,
    legal_time_deviation interval DEFAULT '00:00:00'::interval NOT NULL,
    description text DEFAULT 'No description'::text,
    input_logical_name text NOT NULL,
    owner_user_name text NOT NULL,
    timezone text DEFAULT default_timezone() NOT NULL,
    value_columns text[] DEFAULT ARRAY['high'::text, 'low'::text, 'open'::text, 'close'::text] NOT NULL
);


ALTER TABLE public.input_tables OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 17217)
-- Name: models; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE models (
    model bytea,
    learning_decon_table_column_names text[],
    update_time timestamp without time zone DEFAULT now() NOT NULL,
    literal_decon_table_name text,
    model_id bigint NOT NULL
);


ALTER TABLE public.models OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 78744)
-- Name: models_model_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE models_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.models_model_id_seq OWNER TO postgres;

--
-- TOC entry 2390 (class 0 OID 0)
-- Dependencies: 193
-- Name: models_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE models_model_id_seq OWNED BY models.model_id;


--
-- TOC entry 175 (class 1259 OID 17259)
-- Name: session; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE session (
    key text NOT NULL,
    value text NOT NULL,
    expiry bigint
);


ALTER TABLE public.session OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 78407)
-- Name: tabletest; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tabletest (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    salary integer DEFAULT 0,
    birth_date timestamp without time zone
);


ALTER TABLE public.tabletest OWNER TO postgres;

--
-- TOC entry 168 (class 1259 OID 16469)
-- Name: template_table_input_values; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE template_table_input_values (
    value_time timestamp without time zone DEFAULT date_trunc('seconds'::text, now()) NOT NULL,
    update_time timestamp without time zone DEFAULT date_trunc('seconds'::text, now()),
    weight double precision DEFAULT 1,
    is_final boolean DEFAULT false NOT NULL
);


ALTER TABLE public.template_table_input_values OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 17301)
-- Name: template_table_request; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE template_table_request (
    request_time timestamp without time zone DEFAULT date_trunc('seconds'::text, now()),
    user_name text NOT NULL,
    queue_id text NOT NULL,
    request_id integer NOT NULL,
    request_type integer,
    requested_value_time_coordinate timestamp without time zone NOT NULL,
    requested_value integer
);


ALTER TABLE public.template_table_request OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 17308)
-- Name: template_table_request_request_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE template_table_request_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.template_table_request_request_id_seq OWNER TO postgres;

--
-- TOC entry 2391 (class 0 OID 0)
-- Dependencies: 177
-- Name: template_table_request_request_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE template_table_request_request_id_seq OWNED BY template_table_request.request_id;


--
-- TOC entry 178 (class 1259 OID 17310)
-- Name: update_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE update_log (
    last_update timestamp without time zone DEFAULT now() NOT NULL,
    input_table text NOT NULL
);


ALTER TABLE public.update_log OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 17317)
-- Name: valid_users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE valid_users (
    username text NOT NULL,
    password text NOT NULL,
    email text NOT NULL,
    realname text,
    surname text,
    user_id bigint NOT NULL,
    login text,
    disabled boolean DEFAULT false,
    CONSTRAINT valid_username CHECK ((username ~* '^[a-z][a-z0-9]{3,20}$'::text))
);


ALTER TABLE public.valid_users OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 17324)
-- Name: valid_users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE valid_users_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valid_users_user_id_seq OWNER TO postgres;

--
-- TOC entry 2393 (class 0 OID 0)
-- Dependencies: 180
-- Name: valid_users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE valid_users_user_id_seq OWNED BY valid_users.user_id;


--
-- TOC entry 183 (class 1259 OID 53704)
-- Name: vgfeit_302; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vgfeit_302 (
    open double precision DEFAULT 0,
    high double precision DEFAULT 0,
    low double precision DEFAULT 0,
    close double precision DEFAULT 0,
    volume double precision DEFAULT 0
)
INHERITS (template_table_input_values);


ALTER TABLE public.vgfeit_302 OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 62086)
-- Name: vgfeit_312; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vgfeit_312 (
    open double precision DEFAULT 0,
    high double precision DEFAULT 0,
    low double precision DEFAULT 0,
    close double precision DEFAULT 0,
    volume double precision DEFAULT 0
)
INHERITS (template_table_input_values);


ALTER TABLE public.vgfeit_312 OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 62105)
-- Name: vgfeit_313; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vgfeit_313 (
    open double precision DEFAULT 0,
    high double precision DEFAULT 0,
    low double precision DEFAULT 0,
    close double precision DEFAULT 0,
    volume double precision DEFAULT 0
)
INHERITS (template_table_input_values);


ALTER TABLE public.vgfeit_313 OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 62124)
-- Name: vgfeit_314; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vgfeit_314 (
    open double precision DEFAULT 0,
    high double precision DEFAULT 0,
    low double precision DEFAULT 0,
    close double precision DEFAULT 0,
    volume double precision DEFAULT 0
)
INHERITS (template_table_input_values);


ALTER TABLE public.vgfeit_314 OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 62143)
-- Name: vgfeit_315; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vgfeit_315 (
    open double precision DEFAULT 0,
    high double precision DEFAULT 0,
    low double precision DEFAULT 0,
    close double precision DEFAULT 0,
    volume double precision DEFAULT 0
)
INHERITS (template_table_input_values);


ALTER TABLE public.vgfeit_315 OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 62162)
-- Name: vgfeit_316; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vgfeit_316 (
    open double precision DEFAULT 0,
    high double precision DEFAULT 0,
    low double precision DEFAULT 0,
    close double precision DEFAULT 0,
    volume double precision DEFAULT 0
)
INHERITS (template_table_input_values);


ALTER TABLE public.vgfeit_316 OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 62181)
-- Name: vgfeit_317; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vgfeit_317 (
    open double precision DEFAULT 0,
    high double precision DEFAULT 0,
    low double precision DEFAULT 0,
    close double precision DEFAULT 0,
    volume double precision DEFAULT 0
)
INHERITS (template_table_input_values);


ALTER TABLE public.vgfeit_317 OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 62200)
-- Name: vgfeit_318; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vgfeit_318 (
    open double precision DEFAULT 0,
    high double precision DEFAULT 0,
    low double precision DEFAULT 0,
    close double precision DEFAULT 0,
    volume double precision DEFAULT 0
)
INHERITS (template_table_input_values);


ALTER TABLE public.vgfeit_318 OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 62220)
-- Name: vgfeit_319; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vgfeit_319 (
    open double precision DEFAULT 0,
    high double precision DEFAULT 0,
    low double precision DEFAULT 0,
    close double precision DEFAULT 0,
    volume double precision DEFAULT 0
)
INHERITS (template_table_input_values);


ALTER TABLE public.vgfeit_319 OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 78863)
-- Name: vgfeit_321; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vgfeit_321 (
    high double precision DEFAULT 0,
    low double precision DEFAULT 0,
    open double precision DEFAULT 0,
    close double precision DEFAULT 0
)
INHERITS (template_table_input_values);


ALTER TABLE public.vgfeit_321 OWNER TO postgres;

--
-- TOC entry 2156 (class 2604 OID 17346)
-- Name: dataset_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY datasets ALTER COLUMN dataset_id SET DEFAULT nextval('datasets_dataset_id_seq'::regclass);


--
-- TOC entry 2150 (class 2604 OID 78746)
-- Name: model_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY models ALTER COLUMN model_id SET DEFAULT nextval('models_model_id_seq'::regclass);


--
-- TOC entry 2167 (class 2604 OID 17349)
-- Name: request_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY template_table_request ALTER COLUMN request_id SET DEFAULT nextval('template_table_request_request_id_seq'::regclass);


--
-- TOC entry 2169 (class 2604 OID 17350)
-- Name: user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY valid_users ALTER COLUMN user_id SET DEFAULT nextval('valid_users_user_id_seq'::regclass);


--
-- TOC entry 2173 (class 2604 OID 53707)
-- Name: value_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_302 ALTER COLUMN value_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2174 (class 2604 OID 53708)
-- Name: update_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_302 ALTER COLUMN update_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2175 (class 2604 OID 53709)
-- Name: weight; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_302 ALTER COLUMN weight SET DEFAULT 1;


--
-- TOC entry 2176 (class 2604 OID 53710)
-- Name: is_final; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_302 ALTER COLUMN is_final SET DEFAULT false;


--
-- TOC entry 2182 (class 2604 OID 62089)
-- Name: value_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_312 ALTER COLUMN value_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2183 (class 2604 OID 62090)
-- Name: update_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_312 ALTER COLUMN update_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2184 (class 2604 OID 62091)
-- Name: weight; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_312 ALTER COLUMN weight SET DEFAULT 1;


--
-- TOC entry 2185 (class 2604 OID 62092)
-- Name: is_final; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_312 ALTER COLUMN is_final SET DEFAULT false;


--
-- TOC entry 2191 (class 2604 OID 62108)
-- Name: value_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_313 ALTER COLUMN value_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2192 (class 2604 OID 62109)
-- Name: update_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_313 ALTER COLUMN update_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2193 (class 2604 OID 62110)
-- Name: weight; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_313 ALTER COLUMN weight SET DEFAULT 1;


--
-- TOC entry 2194 (class 2604 OID 62111)
-- Name: is_final; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_313 ALTER COLUMN is_final SET DEFAULT false;


--
-- TOC entry 2200 (class 2604 OID 62127)
-- Name: value_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_314 ALTER COLUMN value_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2201 (class 2604 OID 62128)
-- Name: update_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_314 ALTER COLUMN update_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2202 (class 2604 OID 62129)
-- Name: weight; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_314 ALTER COLUMN weight SET DEFAULT 1;


--
-- TOC entry 2203 (class 2604 OID 62130)
-- Name: is_final; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_314 ALTER COLUMN is_final SET DEFAULT false;


--
-- TOC entry 2209 (class 2604 OID 62146)
-- Name: value_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_315 ALTER COLUMN value_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2210 (class 2604 OID 62147)
-- Name: update_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_315 ALTER COLUMN update_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2211 (class 2604 OID 62148)
-- Name: weight; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_315 ALTER COLUMN weight SET DEFAULT 1;


--
-- TOC entry 2212 (class 2604 OID 62149)
-- Name: is_final; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_315 ALTER COLUMN is_final SET DEFAULT false;


--
-- TOC entry 2218 (class 2604 OID 62165)
-- Name: value_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_316 ALTER COLUMN value_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2219 (class 2604 OID 62166)
-- Name: update_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_316 ALTER COLUMN update_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2220 (class 2604 OID 62167)
-- Name: weight; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_316 ALTER COLUMN weight SET DEFAULT 1;


--
-- TOC entry 2221 (class 2604 OID 62168)
-- Name: is_final; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_316 ALTER COLUMN is_final SET DEFAULT false;


--
-- TOC entry 2227 (class 2604 OID 62184)
-- Name: value_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_317 ALTER COLUMN value_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2228 (class 2604 OID 62185)
-- Name: update_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_317 ALTER COLUMN update_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2229 (class 2604 OID 62186)
-- Name: weight; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_317 ALTER COLUMN weight SET DEFAULT 1;


--
-- TOC entry 2230 (class 2604 OID 62187)
-- Name: is_final; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_317 ALTER COLUMN is_final SET DEFAULT false;


--
-- TOC entry 2236 (class 2604 OID 62203)
-- Name: value_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_318 ALTER COLUMN value_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2237 (class 2604 OID 62204)
-- Name: update_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_318 ALTER COLUMN update_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2238 (class 2604 OID 62205)
-- Name: weight; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_318 ALTER COLUMN weight SET DEFAULT 1;


--
-- TOC entry 2239 (class 2604 OID 62206)
-- Name: is_final; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_318 ALTER COLUMN is_final SET DEFAULT false;


--
-- TOC entry 2245 (class 2604 OID 62223)
-- Name: value_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_319 ALTER COLUMN value_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2246 (class 2604 OID 62224)
-- Name: update_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_319 ALTER COLUMN update_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2247 (class 2604 OID 62225)
-- Name: weight; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_319 ALTER COLUMN weight SET DEFAULT 1;


--
-- TOC entry 2248 (class 2604 OID 62226)
-- Name: is_final; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_319 ALTER COLUMN is_final SET DEFAULT false;


--
-- TOC entry 2255 (class 2604 OID 78866)
-- Name: value_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_321 ALTER COLUMN value_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2256 (class 2604 OID 78867)
-- Name: update_time; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_321 ALTER COLUMN update_time SET DEFAULT date_trunc('seconds'::text, now());


--
-- TOC entry 2257 (class 2604 OID 78868)
-- Name: weight; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_321 ALTER COLUMN weight SET DEFAULT 1;


--
-- TOC entry 2258 (class 2604 OID 78869)
-- Name: is_final; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vgfeit_321 ALTER COLUMN is_final SET DEFAULT false;


--
-- TOC entry 2314 (class 2606 OID 78412)
-- Name: PK_IDT; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tabletest
    ADD CONSTRAINT "PK_IDT" PRIMARY KEY (id);


--
-- TOC entry 2267 (class 2606 OID 17360)
-- Name: dataset_input_tables_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dataset_input_tables
    ADD CONSTRAINT dataset_input_tables_pkey PRIMARY KEY (dataset_id, children_input_table);


--
-- TOC entry 2271 (class 2606 OID 17364)
-- Name: datasets_dataset_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY datasets
    ADD CONSTRAINT datasets_dataset_id_key UNIQUE (dataset_id);


--
-- TOC entry 2273 (class 2606 OID 17366)
-- Name: datasets_dataset_name_owner_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY datasets
    ADD CONSTRAINT datasets_dataset_name_owner_user_id_key UNIQUE (dataset_name, owner_user_id);


--
-- TOC entry 2275 (class 2606 OID 17368)
-- Name: datasets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY datasets
    ADD CONSTRAINT datasets_pkey PRIMARY KEY (dataset_id);


--
-- TOC entry 2294 (class 2606 OID 78384)
-- Name: input_decons_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY input_decons
    ADD CONSTRAINT input_decons_pkey PRIMARY KEY (literal_decon_table);


--
-- TOC entry 2277 (class 2606 OID 17370)
-- Name: input_tables_input_logical_name_owner_user_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY input_tables
    ADD CONSTRAINT input_tables_input_logical_name_owner_user_name_key UNIQUE (input_logical_name, owner_user_name);


--
-- TOC entry 2279 (class 2606 OID 17372)
-- Name: input_tables_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY input_tables
    ADD CONSTRAINT input_tables_pkey PRIMARY KEY (input_table_physical_name);


--
-- TOC entry 2269 (class 2606 OID 78759)
-- Name: models_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY models
    ADD CONSTRAINT models_pkey PRIMARY KEY (model_id);


--
-- TOC entry 2283 (class 2606 OID 17376)
-- Name: session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY session
    ADD CONSTRAINT session_pkey PRIMARY KEY (key, value);


--
-- TOC entry 2264 (class 2606 OID 17380)
-- Name: template_table_input_values_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY template_table_input_values
    ADD CONSTRAINT template_table_input_values_pkey PRIMARY KEY (value_time);


--
-- TOC entry 2285 (class 2606 OID 17382)
-- Name: template_table_request_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY template_table_request
    ADD CONSTRAINT template_table_request_pk PRIMARY KEY (request_id);


--
-- TOC entry 2281 (class 2606 OID 17384)
-- Name: unique_physical_table_name; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY input_tables
    ADD CONSTRAINT unique_physical_table_name UNIQUE (input_table_physical_name);


--
-- TOC entry 2289 (class 2606 OID 17386)
-- Name: unique_username; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY valid_users
    ADD CONSTRAINT unique_username UNIQUE (username);


--
-- TOC entry 2287 (class 2606 OID 17388)
-- Name: update_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY update_log
    ADD CONSTRAINT update_log_pkey PRIMARY KEY (input_table);


--
-- TOC entry 2291 (class 2606 OID 17390)
-- Name: valid_users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY valid_users
    ADD CONSTRAINT valid_users_pkey PRIMARY KEY (user_id);


--
-- TOC entry 2296 (class 2606 OID 53717)
-- Name: vgfeit_302_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vgfeit_302
    ADD CONSTRAINT vgfeit_302_pk PRIMARY KEY (value_time);


--
-- TOC entry 2298 (class 2606 OID 62099)
-- Name: vgfeit_312_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vgfeit_312
    ADD CONSTRAINT vgfeit_312_pk PRIMARY KEY (value_time);


--
-- TOC entry 2300 (class 2606 OID 62118)
-- Name: vgfeit_313_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vgfeit_313
    ADD CONSTRAINT vgfeit_313_pk PRIMARY KEY (value_time);


--
-- TOC entry 2302 (class 2606 OID 62137)
-- Name: vgfeit_314_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vgfeit_314
    ADD CONSTRAINT vgfeit_314_pk PRIMARY KEY (value_time);


--
-- TOC entry 2304 (class 2606 OID 62156)
-- Name: vgfeit_315_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vgfeit_315
    ADD CONSTRAINT vgfeit_315_pk PRIMARY KEY (value_time);


--
-- TOC entry 2306 (class 2606 OID 62175)
-- Name: vgfeit_316_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vgfeit_316
    ADD CONSTRAINT vgfeit_316_pk PRIMARY KEY (value_time);


--
-- TOC entry 2308 (class 2606 OID 62194)
-- Name: vgfeit_317_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vgfeit_317
    ADD CONSTRAINT vgfeit_317_pk PRIMARY KEY (value_time);


--
-- TOC entry 2310 (class 2606 OID 62213)
-- Name: vgfeit_318_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vgfeit_318
    ADD CONSTRAINT vgfeit_318_pk PRIMARY KEY (value_time);


--
-- TOC entry 2312 (class 2606 OID 62233)
-- Name: vgfeit_319_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vgfeit_319
    ADD CONSTRAINT vgfeit_319_pk PRIMARY KEY (value_time);


--
-- TOC entry 2316 (class 2606 OID 78875)
-- Name: vgfeit_321_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vgfeit_321
    ADD CONSTRAINT vgfeit_321_pk PRIMARY KEY (value_time);


--
-- TOC entry 2292 (class 1259 OID 78352)
-- Name: input_decons_literal_input_table_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX input_decons_literal_input_table_idx ON input_decons USING btree (literal_input_table);


--
-- TOC entry 2265 (class 1259 OID 17399)
-- Name: trpe_value_time_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX trpe_value_time_idx ON template_table_input_values USING btree (value_time DESC NULLS LAST);


--
-- TOC entry 2330 (class 2620 OID 53722)
-- Name: forbid_key_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER forbid_key_update BEFORE UPDATE ON vgfeit_302 FOR EACH ROW EXECUTE PROCEDURE constraint_key_update();


--
-- TOC entry 2335 (class 2620 OID 62104)
-- Name: forbid_key_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER forbid_key_update BEFORE UPDATE ON vgfeit_312 FOR EACH ROW EXECUTE PROCEDURE constraint_key_update();


--
-- TOC entry 2340 (class 2620 OID 62123)
-- Name: forbid_key_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER forbid_key_update BEFORE UPDATE ON vgfeit_313 FOR EACH ROW EXECUTE PROCEDURE constraint_key_update();


--
-- TOC entry 2345 (class 2620 OID 62142)
-- Name: forbid_key_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER forbid_key_update BEFORE UPDATE ON vgfeit_314 FOR EACH ROW EXECUTE PROCEDURE constraint_key_update();


--
-- TOC entry 2350 (class 2620 OID 62161)
-- Name: forbid_key_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER forbid_key_update BEFORE UPDATE ON vgfeit_315 FOR EACH ROW EXECUTE PROCEDURE constraint_key_update();


--
-- TOC entry 2355 (class 2620 OID 62180)
-- Name: forbid_key_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER forbid_key_update BEFORE UPDATE ON vgfeit_316 FOR EACH ROW EXECUTE PROCEDURE constraint_key_update();


--
-- TOC entry 2360 (class 2620 OID 62199)
-- Name: forbid_key_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER forbid_key_update BEFORE UPDATE ON vgfeit_317 FOR EACH ROW EXECUTE PROCEDURE constraint_key_update();


--
-- TOC entry 2365 (class 2620 OID 62218)
-- Name: forbid_key_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER forbid_key_update BEFORE UPDATE ON vgfeit_318 FOR EACH ROW EXECUTE PROCEDURE constraint_key_update();


--
-- TOC entry 2370 (class 2620 OID 62238)
-- Name: forbid_key_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER forbid_key_update BEFORE UPDATE ON vgfeit_319 FOR EACH ROW EXECUTE PROCEDURE constraint_key_update();


--
-- TOC entry 2375 (class 2620 OID 78880)
-- Name: forbid_key_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER forbid_key_update BEFORE UPDATE ON vgfeit_321 FOR EACH ROW EXECUTE PROCEDURE constraint_key_update();


--
-- TOC entry 2322 (class 2620 OID 17406)
-- Name: grid_value_time; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER grid_value_time BEFORE UPDATE OF value_time ON template_table_input_values FOR EACH ROW EXECUTE PROCEDURE row_on_time_grid();


--
-- TOC entry 2326 (class 2620 OID 53721)
-- Name: on_final_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_final_value_update BEFORE UPDATE ON vgfeit_302 FOR EACH ROW EXECUTE PROCEDURE check_final_values_update();


--
-- TOC entry 2334 (class 2620 OID 62103)
-- Name: on_final_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_final_value_update BEFORE UPDATE ON vgfeit_312 FOR EACH ROW EXECUTE PROCEDURE check_final_values_update();


--
-- TOC entry 2339 (class 2620 OID 62122)
-- Name: on_final_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_final_value_update BEFORE UPDATE ON vgfeit_313 FOR EACH ROW EXECUTE PROCEDURE check_final_values_update();


--
-- TOC entry 2344 (class 2620 OID 62141)
-- Name: on_final_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_final_value_update BEFORE UPDATE ON vgfeit_314 FOR EACH ROW EXECUTE PROCEDURE check_final_values_update();


--
-- TOC entry 2349 (class 2620 OID 62160)
-- Name: on_final_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_final_value_update BEFORE UPDATE ON vgfeit_315 FOR EACH ROW EXECUTE PROCEDURE check_final_values_update();


--
-- TOC entry 2354 (class 2620 OID 62179)
-- Name: on_final_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_final_value_update BEFORE UPDATE ON vgfeit_316 FOR EACH ROW EXECUTE PROCEDURE check_final_values_update();


--
-- TOC entry 2359 (class 2620 OID 62198)
-- Name: on_final_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_final_value_update BEFORE UPDATE ON vgfeit_317 FOR EACH ROW EXECUTE PROCEDURE check_final_values_update();


--
-- TOC entry 2364 (class 2620 OID 62217)
-- Name: on_final_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_final_value_update BEFORE UPDATE ON vgfeit_318 FOR EACH ROW EXECUTE PROCEDURE check_final_values_update();


--
-- TOC entry 2369 (class 2620 OID 62237)
-- Name: on_final_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_final_value_update BEFORE UPDATE ON vgfeit_319 FOR EACH ROW EXECUTE PROCEDURE check_final_values_update();


--
-- TOC entry 2374 (class 2620 OID 78879)
-- Name: on_final_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_final_value_update BEFORE UPDATE ON vgfeit_321 FOR EACH ROW EXECUTE PROCEDURE check_final_values_update();


--
-- TOC entry 2323 (class 2620 OID 17410)
-- Name: on_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_value_update AFTER UPDATE ON template_table_input_values FOR EACH ROW EXECUTE PROCEDURE stamp_update();


--
-- TOC entry 2327 (class 2620 OID 53718)
-- Name: on_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_value_update AFTER UPDATE ON vgfeit_302 FOR EACH ROW EXECUTE PROCEDURE stamp_update();


--
-- TOC entry 2331 (class 2620 OID 62100)
-- Name: on_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_value_update AFTER UPDATE ON vgfeit_312 FOR EACH ROW EXECUTE PROCEDURE stamp_update();


--
-- TOC entry 2336 (class 2620 OID 62119)
-- Name: on_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_value_update AFTER UPDATE ON vgfeit_313 FOR EACH ROW EXECUTE PROCEDURE stamp_update();


--
-- TOC entry 2341 (class 2620 OID 62138)
-- Name: on_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_value_update AFTER UPDATE ON vgfeit_314 FOR EACH ROW EXECUTE PROCEDURE stamp_update();


--
-- TOC entry 2346 (class 2620 OID 62157)
-- Name: on_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_value_update AFTER UPDATE ON vgfeit_315 FOR EACH ROW EXECUTE PROCEDURE stamp_update();


--
-- TOC entry 2351 (class 2620 OID 62176)
-- Name: on_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_value_update AFTER UPDATE ON vgfeit_316 FOR EACH ROW EXECUTE PROCEDURE stamp_update();


--
-- TOC entry 2356 (class 2620 OID 62195)
-- Name: on_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_value_update AFTER UPDATE ON vgfeit_317 FOR EACH ROW EXECUTE PROCEDURE stamp_update();


--
-- TOC entry 2361 (class 2620 OID 62214)
-- Name: on_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_value_update AFTER UPDATE ON vgfeit_318 FOR EACH ROW EXECUTE PROCEDURE stamp_update();


--
-- TOC entry 2366 (class 2620 OID 62234)
-- Name: on_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_value_update AFTER UPDATE ON vgfeit_319 FOR EACH ROW EXECUTE PROCEDURE stamp_update();


--
-- TOC entry 2371 (class 2620 OID 78876)
-- Name: on_value_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER on_value_update AFTER UPDATE ON vgfeit_321 FOR EACH ROW EXECUTE PROCEDURE stamp_update();


--
-- TOC entry 2324 (class 2620 OID 17416)
-- Name: time_grid_check; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER time_grid_check BEFORE INSERT OR UPDATE ON template_table_input_values FOR EACH ROW EXECUTE PROCEDURE row_on_time_grid();


--
-- TOC entry 2329 (class 2620 OID 53720)
-- Name: time_grid_check; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER time_grid_check BEFORE INSERT OR UPDATE ON vgfeit_302 FOR EACH ROW EXECUTE PROCEDURE row_on_time_grid();


--
-- TOC entry 2333 (class 2620 OID 62102)
-- Name: time_grid_check; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER time_grid_check BEFORE INSERT OR UPDATE ON vgfeit_312 FOR EACH ROW EXECUTE PROCEDURE row_on_time_grid();


--
-- TOC entry 2338 (class 2620 OID 62121)
-- Name: time_grid_check; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER time_grid_check BEFORE INSERT OR UPDATE ON vgfeit_313 FOR EACH ROW EXECUTE PROCEDURE row_on_time_grid();


--
-- TOC entry 2343 (class 2620 OID 62140)
-- Name: time_grid_check; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER time_grid_check BEFORE INSERT OR UPDATE ON vgfeit_314 FOR EACH ROW EXECUTE PROCEDURE row_on_time_grid();


--
-- TOC entry 2348 (class 2620 OID 62159)
-- Name: time_grid_check; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER time_grid_check BEFORE INSERT OR UPDATE ON vgfeit_315 FOR EACH ROW EXECUTE PROCEDURE row_on_time_grid();


--
-- TOC entry 2353 (class 2620 OID 62178)
-- Name: time_grid_check; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER time_grid_check BEFORE INSERT OR UPDATE ON vgfeit_316 FOR EACH ROW EXECUTE PROCEDURE row_on_time_grid();


--
-- TOC entry 2358 (class 2620 OID 62197)
-- Name: time_grid_check; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER time_grid_check BEFORE INSERT OR UPDATE ON vgfeit_317 FOR EACH ROW EXECUTE PROCEDURE row_on_time_grid();


--
-- TOC entry 2363 (class 2620 OID 62216)
-- Name: time_grid_check; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER time_grid_check BEFORE INSERT OR UPDATE ON vgfeit_318 FOR EACH ROW EXECUTE PROCEDURE row_on_time_grid();


--
-- TOC entry 2368 (class 2620 OID 62236)
-- Name: time_grid_check; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER time_grid_check BEFORE INSERT OR UPDATE ON vgfeit_319 FOR EACH ROW EXECUTE PROCEDURE row_on_time_grid();


--
-- TOC entry 2373 (class 2620 OID 78878)
-- Name: time_grid_check; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER time_grid_check BEFORE INSERT OR UPDATE ON vgfeit_321 FOR EACH ROW EXECUTE PROCEDURE row_on_time_grid();


--
-- TOC entry 2325 (class 2620 OID 17420)
-- Name: update_logger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_logger AFTER INSERT OR DELETE OR UPDATE ON template_table_input_values FOR EACH STATEMENT EXECUTE PROCEDURE log_queue_updates();


--
-- TOC entry 2328 (class 2620 OID 53719)
-- Name: update_logger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_logger AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON vgfeit_302 FOR EACH STATEMENT EXECUTE PROCEDURE log_queue_updates();


--
-- TOC entry 2332 (class 2620 OID 62101)
-- Name: update_logger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_logger AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON vgfeit_312 FOR EACH STATEMENT EXECUTE PROCEDURE log_queue_updates();


--
-- TOC entry 2337 (class 2620 OID 62120)
-- Name: update_logger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_logger AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON vgfeit_313 FOR EACH STATEMENT EXECUTE PROCEDURE log_queue_updates();


--
-- TOC entry 2342 (class 2620 OID 62139)
-- Name: update_logger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_logger AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON vgfeit_314 FOR EACH STATEMENT EXECUTE PROCEDURE log_queue_updates();


--
-- TOC entry 2347 (class 2620 OID 62158)
-- Name: update_logger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_logger AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON vgfeit_315 FOR EACH STATEMENT EXECUTE PROCEDURE log_queue_updates();


--
-- TOC entry 2352 (class 2620 OID 62177)
-- Name: update_logger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_logger AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON vgfeit_316 FOR EACH STATEMENT EXECUTE PROCEDURE log_queue_updates();


--
-- TOC entry 2357 (class 2620 OID 62196)
-- Name: update_logger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_logger AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON vgfeit_317 FOR EACH STATEMENT EXECUTE PROCEDURE log_queue_updates();


--
-- TOC entry 2362 (class 2620 OID 62215)
-- Name: update_logger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_logger AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON vgfeit_318 FOR EACH STATEMENT EXECUTE PROCEDURE log_queue_updates();


--
-- TOC entry 2367 (class 2620 OID 62235)
-- Name: update_logger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_logger AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON vgfeit_319 FOR EACH STATEMENT EXECUTE PROCEDURE log_queue_updates();


--
-- TOC entry 2372 (class 2620 OID 78877)
-- Name: update_logger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_logger AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON vgfeit_321 FOR EACH STATEMENT EXECUTE PROCEDURE log_queue_updates();


--
-- TOC entry 2317 (class 2606 OID 42480)
-- Name: dataset_input_tables_children_input_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dataset_input_tables
    ADD CONSTRAINT dataset_input_tables_children_input_table_fkey FOREIGN KEY (children_input_table) REFERENCES input_tables(input_table_physical_name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2318 (class 2606 OID 42485)
-- Name: dataset_input_tables_dataset_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dataset_input_tables
    ADD CONSTRAINT dataset_input_tables_dataset_id_fkey FOREIGN KEY (dataset_id) REFERENCES datasets(dataset_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2320 (class 2606 OID 78329)
-- Name: datasets_owner_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY datasets
    ADD CONSTRAINT datasets_owner_user_id_fkey FOREIGN KEY (owner_user_id) REFERENCES valid_users(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2321 (class 2606 OID 78378)
-- Name: input_decons_literal_input_table_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY input_decons
    ADD CONSTRAINT input_decons_literal_input_table_fkey FOREIGN KEY (literal_input_table) REFERENCES input_tables(input_table_physical_name) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2319 (class 2606 OID 78753)
-- Name: models_literal_decon_table_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY models
    ADD CONSTRAINT models_literal_decon_table_name_fkey FOREIGN KEY (literal_decon_table_name) REFERENCES input_decons(literal_decon_table) ON UPDATE CASCADE;


--
-- TOC entry 2383 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 2392 (class 0 OID 0)
-- Dependencies: 179
-- Name: valid_users; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE valid_users FROM PUBLIC;
REVOKE ALL ON TABLE valid_users FROM postgres;
GRANT ALL ON TABLE valid_users TO postgres;
GRANT SELECT ON TABLE valid_users TO pguser;


-- Completed on 2013-12-05 00:28:53 CET

--
-- PostgreSQL database dump complete
--

