\o 27042014_Create_input_and_decon_table_functions_update.log

-- Function: create_decon_table(text, bigint, text, text, smallint, text)

-- DROP FUNCTION create_decon_table(text, bigint, text, text, smallint, text);

CREATE OR REPLACE FUNCTION create_decon_table(user_name text, dataset_id bigint, queue_table_name text, queue_column text, swt_level smallint, wavelet text)
  RETURNS text AS
$BODY$DECLARE unq_name text := '';
	qry text;
	level_cnt smallint := 0;
BEGIN

	IF NOT EXISTS(SELECT 1 FROM datasets, valid_users AS vu WHERE datasets.dataset_id = $2 
		AND vu.username = $1 AND vu.user_id = datasets.owner_user_id) 	
	OR NOT EXISTS(SELECT 1 FROM information_schema.columns as isc
		WHERE isc.table_name = $3 AND isc.column_name = $4)
	THEN RAISE EXCEPTION 'The given arguments are invalid to further process the request.'; 
	RETURN ''; END IF;
27042014_Create_input_and_decon_table_functions_update
	IF NOT EXISTS(SELECT 1 FROM dataset_input_tables as dit WHERE dit.dataset_id = $2 
		AND children_input_table = $3) THEN
		RAISE EXCEPTION 'The given input table % doesn''t belong to the specified dataset with id %.', $3, $2;
		RETURN ''; END IF;

	BEGIN
		SELECT literal_decon_table INTO STRICT unq_name FROM input_decons 
		WHERE literal_input_table = queue_table_name	
		AND source_column = queue_column
		AND input_decons.dataset_id = $2
		AND input_decons.swt_level = $5;

		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			unq_name := 'd_' || $1 || '_' || $2 || '_' || $3 || '_' || $4;
			qry := 'CREATE TABLE ' || unq_name || 
			'( value_time timestamp without time zone not null, ';		
			LOOP
				EXIT WHEN level_cnt > swt_level;
				qry := qry || 'level' || level_cnt || ' double precision default 0, ';
				level_cnt := level_cnt + 1;

			END LOOP;

			qry := qry || 'update_time timestamp without time zone default date_trunc(''seconds''::text, now()), 
				CONSTRAINT ' || unq_name || '_pk PRIMARY KEY(value_time))
				WITH (OIDS = FALSE);	
				CREATE TRIGGER on_value_update
				AFTER UPDATE
				ON ' || unq_name || '
				FOR EACH ROW
				EXECUTE PROCEDURE stamp_update();';

			EXECUTE qry;	

			INSERT INTO input_decons VALUES(queue_table_name, queue_column, wavelet, unq_name, dataset_id, swt_level);
			RETURN unq_name;
		WHEN TOO_MANY_ROWS THEN
			RAISE EXCEPTION 'Two or more Decon tables exist in database with name %! Critical failure!', unq_name;
			RETURN unq_name;
	END;
	RETURN unq_name;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 10;
ALTER FUNCTION create_decon_table(text, bigint, text, text, smallint, text)
  OWNER TO postgres;





-- Function: create_input_table(text, text, text[], interval, interval, text, text)

-- DROP FUNCTION create_input_table(text, text, text[], interval, interval, text, text);

CREATE OR REPLACE FUNCTION create_input_table(owner_user_name text, input_table_name text, values_columns text[], resolution interval DEFAULT '00:00:01'::interval, legal_time_deviation interval DEFAULT '00:00:00'::interval, timezone text DEFAULT default_timezone(), description text DEFAULT 'Unnamed queue'::text)
  RETURNS text AS
$BODY$
DECLARE q text;
DECLARE cnt integer := 1;
DECLARE uniq_tab_name text;
BEGIN

uniq_tab_name := 'q_' || quote_ident(owner_user_name) || '_' || nextval('input_table_name_unique_part')::text;

q := 'CREATE TABLE ' || uniq_tab_name || '( '; 

LOOP
	EXIT WHEN cnt > array_length(values_columns, 1);
	q := q || quote_ident(values_columns[cnt]) || ' double precision default 0, ';
	cnt := cnt + 1;
END LOOP;

q := q || ' CONSTRAINT ' || uniq_tab_name || '_pk PRIMARY KEY (value_time))
	INHERITS (template_table_input_values)
	WITH (
	 OIDS=FALSE
	);';


q := q || 'CREATE TRIGGER on_value_update
	AFTER UPDATE
	ON ' || uniq_tab_name || '
	FOR EACH ROW
	EXECUTE PROCEDURE stamp_update();';

q := q || 'CREATE TRIGGER update_logger
	AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
	ON ' || uniq_tab_name || '
	FOR EACH STATEMENT
	EXECUTE PROCEDURE log_queue_updates();';

q := q || 'CREATE TRIGGER time_grid_check
	BEFORE INSERT OR UPDATE
	ON ' || uniq_tab_name || '
	FOR EACH ROW
	EXECUTE PROCEDURE row_on_time_grid();';

q := q || 'CREATE TRIGGER on_final_value_update
	BEFORE UPDATE
	ON ' || uniq_tab_name || '
	FOR EACH ROW
	EXECUTE PROCEDURE  check_final_values_update();';

q := q || 'CREATE TRIGGER forbid_key_update
	BEFORE UPDATE
	ON ' || uniq_tab_name || '
	FOR EACH ROW
	EXECUTE PROCEDURE constraint_key_update();';
  

EXECUTE q;

INSERT INTO input_tables VALUES(uniq_tab_name, resolution, legal_time_deviation, 
	description, input_table_name, owner_user_name, timezone);

RETURN uniq_tab_name;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 1;
ALTER FUNCTION create_input_table(text, text, text[], interval, interval, text, text)
  OWNER TO postgres;


ALTER TABLE input_decons RENAME literal_input_table  TO input_table_physical_name;
ALTER TABLE input_decons RENAME literal_decon_table  TO decon_table_physical_name;
ALTER TABLE input_decons
  DROP CONSTRAINT input_decons_pkey;
ALTER TABLE input_decons
  DROP CONSTRAINT input_decons_literal_input_table_fkey;
ALTER TABLE input_decons
  ADD PRIMARY KEY (decon_table_physical_name);
ALTER TABLE input_decons
  ADD FOREIGN KEY (input_table_physical_name) REFERENCES input_tables (input_table_physical_name) ON UPDATE CASCADE ON DELETE NO ACTION;


commit;

\o

