\o 27042014_Renaming_of_input_decons_fields.log

ALTER TABLE input_decons RENAME literal_input_table  TO input_table_physical_name;
ALTER TABLE input_decons RENAME literal_decon_table  TO decon_table_physical_name;
ALTER TABLE input_decons
  DROP CONSTRAINT input_decons_pkey;
ALTER TABLE input_decons
  DROP CONSTRAINT input_decons_literal_input_table_fkey;
ALTER TABLE input_decons
  ADD PRIMARY KEY (decon_table_physical_name);
ALTER TABLE input_decons
  ADD FOREIGN KEY (input_table_physical_name) REFERENCES input_tables (input_table_physical_name) ON UPDATE CASCADE ON DELETE NO ACTION;

commit;

\o
