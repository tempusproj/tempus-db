﻿-- Table: multival_requests

\connect :SVRDB

DROP TABLE IF EXISTS :"SVRSCHEMA".multival_results;

DROP TABLE IF EXISTS :"SVRSCHEMA".multival_requests;

CREATE TABLE :"SVRSCHEMA".multival_requests
(
  request_id bigint NOT NULL,
  dataset_id bigint NOT NULL, -- Dataset this request is associated to.
  user_name text, -- user making this request. Authentication and verification is handled at the web service layer.
  request_time timestamp without time zone NOT NULL DEFAULT now(), -- Time when request was made.
  value_time_start timestamp without time zone NOT NULL, -- Prediction request range start.
  value_time_end timestamp without time zone NOT NULL, -- Prediction request range start.
  resolution integer DEFAULT 60,
  value_columns text[],
  processed boolean DEFAULT false,
    
  CONSTRAINT pk_requests PRIMARY KEY (request_id),
  CONSTRAINT fk_multival_requests_dataset_id FOREIGN KEY (dataset_id)
      REFERENCES datasets (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_multival_requests_user_name FOREIGN KEY (user_name)
      REFERENCES userauth (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE :"SVRSCHEMA".multival_requests
  OWNER TO :SVRUSER;
COMMENT ON TABLE :"SVRSCHEMA".multival_requests IS 'Contains multiple value requests.';
COMMENT ON COLUMN :"SVRSCHEMA".multival_requests.value_time_start IS 'The first requested time.';
COMMENT ON COLUMN :"SVRSCHEMA".multival_requests.resolution IS 'One interval length in seconds.';

-- Index: i_multival_requests_processed

DROP INDEX IF EXISTS i_multival_requests_processed;

CREATE INDEX i_multival_requests_processed
  ON multival_requests
  USING btree(processed);


-- Index: fki_multival_requests_dataset_id

DROP INDEX IF EXISTS :"SVRSCHEMA".fki_multival_requests_dataset_id;

CREATE INDEX fki_multival_requests_dataset_id
  ON multival_requests
  USING btree(dataset_id);

-- Index: fki_multival_requests_value_time_start

DROP INDEX IF EXISTS fki_multival_requests_value_time_start;

CREATE INDEX  fki_multival_requests_value_time_start
  ON :"SVRSCHEMA".multival_requests
  USING btree(value_time_start, value_time_end, resolution);


-- Index: fki_multival_requests_user_name

DROP INDEX IF EXISTS fki_multival_requests_user_name;

CREATE INDEX fki_multival_requests_user_name
  ON :"SVRSCHEMA".multival_requests
  USING btree(user_name);

-- Table: multival_results

CREATE TABLE :"SVRSCHEMA".multival_results
(
  response_id bigint NOT NULL,
  request_id bigint NOT NULL,
  value_time timestamp without time zone NOT NULL,
  value_column text NOT NULL,
  value double precision NOT NULL,

  CONSTRAINT pk_responses PRIMARY KEY (response_id),
  CONSTRAINT fk_multival_requests_id FOREIGN KEY (request_id)
      REFERENCES multival_requests (request_id) MATCH SIMPLE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE :"SVRSCHEMA".multival_results
  OWNER TO :SVRUSER;

-- INDEX fki_multival_results_request_id

DROP INDEX IF EXISTS fki_multival_results_request_id;

CREATE INDEX fki_multival_results_request_id
  ON multival_results
  USING btree(request_id);

-- INDEX fki_multival_results_value_time_start  

DROP INDEX IF EXISTS fki_multival_results_value_time_start;

CREATE INDEX  fki_multival_results_value_time_start
  ON :"SVRSCHEMA".multival_results
  USING btree(value_time, value_column);

-- Sequence: response_id_seq

DROP SEQUENCE IF EXISTS :"SVRSCHEMA".result_id_seq;

CREATE SEQUENCE :"SVRSCHEMA".result_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 16932
  CACHE 1;
ALTER TABLE :"SVRSCHEMA".result_id_seq
  OWNER TO :SVRUSER;


