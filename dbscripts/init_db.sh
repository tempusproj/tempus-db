#! /bin/bash

current_path=$PWD
script_path=$(dirname "${BASH_SOURCE[0]}")
default_db=${1:-"svrwave"}
db_version="0.9"

script_vars="-v SVRDB=$default_db -v SVRUSER=$default_db -v SVRPASSWORD=$default_db -v SVRSCHEMA=public"

cd $script_path
if [[ $OSTYPE == darwin* ]]; then
   psql ${script_vars} < ${db_version}_basic_structure.sql
   psql ${script_vars} < ${db_version}_multival_requests.sql
   psql ${script_vars} < ${db_version}_input_queue_missing_hours.sql
   psql ${script_vars} < ${db_version}_eurusd_60_release_data.sql
else
   sudo -u postgres psql ${script_vars} < ${db_version}_basic_structure.sql
   sudo -u postgres psql ${script_vars} < ${db_version}_multival_requests.sql
   sudo -u postgres psql ${script_vars} < ${db_version}_input_queue_missing_hours.sql
   sudo -u postgres psql ${script_vars} < ${db_version}_eurusd_60_release_data.sql
fi
cd $current_path
