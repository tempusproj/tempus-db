#! /bin/bash

current_path=$PWD
script_path=$(dirname "${BASH_SOURCE[0]}")
script_vars="-v SVRDB=svrwave_test -v SVRUSER=svrwave_test -v SVRPASSWORD=svrwave_test -v SVRSCHEMA=public"
cd $script_path
sudo -u postgres psql ${script_vars} < *v0.1.sql
sudo -u postgres psql ${script_vars} < *test_data.sql
cd $current_path