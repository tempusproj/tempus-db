﻿-- Table: multival_requests

\connect :SVRDB


DROP TABLE IF EXISTS :"SVRSCHEMA".input_queue_missing_hours ;

CREATE TABLE :"SVRSCHEMA".input_queue_missing_hours
(
  table_name text NOT NULL,
  start_time timestamp without time zone,
  end_time timestamp without time zone,
  last_request_time timestamp without time zone, -- Last time this interval was requested and failed.
  CONSTRAINT fk_input_queues_table_name FOREIGN KEY (table_name)
      REFERENCES input_queues (table_name) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE :"SVRSCHEMA".input_queue_missing_hours
  OWNER TO :SVRUSER;
COMMENT ON TABLE :"SVRSCHEMA".input_queue_missing_hours  IS 'Contains the time ranges that could not be uploaded to the input queue. Such time ranges may be the exchange closed hours as well as technical errors';
COMMENT ON COLUMN :"SVRSCHEMA".input_queue_missing_hours.last_request_time IS 'Last time this interval was requested and failed.';




DROP INDEX IF EXISTS i_input_queue_missing_hours; 

CREATE INDEX i_input_queue_missing_hours
  ON :"SVRSCHEMA".input_queue_missing_hours
  USING btree
  (table_name COLLATE pg_catalog."default", start_time DESC, end_time DESC);
ALTER TABLE :"SVRSCHEMA".input_queue_missing_hours CLUSTER ON i_input_queue_missing_hours;




CREATE OR REPLACE FUNCTION :"SVRSCHEMA".mark_hours_missing(
    "tableName" text,
    "startTime" timestamp without time zone,
    "endTime" timestamp without time zone)
  RETURNS void AS
$BODY$DECLARE
    rownum integer;
BEGIN
    SELECT INTO rownum count(1)
    FROM input_queue_missing_hours
    WHERE table_name = "tableName" and start_time = "startTime" and end_time = "endTime";

    IF rownum > 0 THEN
	update input_queue_missing_hours set last_request_time = now()
	WHERE table_name = "tableName" and start_time = "startTime" and end_time = "endTime";
    ELSE
	insert into input_queue_missing_hours (table_name, start_time, end_time, last_request_time)
	values ("tableName", "startTime", "endTime", now());
    END IF;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION :"SVRSCHEMA".mark_hours_missing(text, timestamp without time zone, timestamp without time zone)
  OWNER TO :SVRUSER;


