-- ******************************************
-- SVRWAVE Database Structure 
-- Password is "svrwave"
-- ******************************************


DROP DATABASE IF EXISTS :SVRDB;
DROP USER IF EXISTS :SVRUSER;

/*CREATE ROLE :SVRUSER LOGIN ENCRYPTED PASSWORD 'md5048dbe29dc68818f4655c9e677b44b31'
   VALID UNTIL 'infinity';*/

CREATE ROLE :SVRUSER LOGIN PASSWORD :'SVRPASSWORD'
VALID UNTIL 'infinity';

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

CREATE DATABASE :SVRDB WITH
    TEMPLATE = template0
    owner = :SVRUSER
    TABLESPACE = pg_default
    ENCODING = 'UTF8';


\connect :SVRDB

-- CREATE SCHEMA :"SVRSCHEMA" AUTHORIZATION :SVRUSER;

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--ALTER SCHEMA :"SVRSCHEMA" OWNER TO :SVRUSER;
CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';

SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- **********************************
-- CREATE SVRWAVE SYSTEM TABLES     *
-- **********************************

CREATE TABLE :"SVRSCHEMA".userauth
(
   user_id bigint PRIMARY KEY,
   username text NOT NULL,
   password text,
   email text,
   name text,
   role text,
   priority smallint NOT NULL DEFAULT 2,
   CONSTRAINT unique_user_name UNIQUE(username)
)
WITH (
  OIDS = FALSE
)
;
ALTER TABLE :SVRSCHEMA.userauth
  OWNER TO :SVRUSER;


CREATE TABLE :"SVRSCHEMA".input_queues
(
  table_name text PRIMARY KEY,
  logical_name text NOT NULL,
  user_name text NOT NULL,
  description text,
  resolution interval NOT NULL,
  legal_time_deviation interval NOT NULL,
  timezone interval NOT NULL DEFAULT '00:00:00'::interval,
  value_columns text[] NOT NULL,
  missing_hours_retention interval NOT NULL DEFAULT '2 weeks'::interval,
  uses_fix_connection boolean NOT NULL DEFAULT false, 
  CONSTRAINT input_queues_user_name_fkey FOREIGN KEY (user_name)
      REFERENCES :"SVRSCHEMA".userauth (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT input_queues_table_name_user_name_key UNIQUE (logical_name, user_name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE :"SVRSCHEMA".input_queues
  OWNER TO :SVRUSER;


CREATE TABLE :"SVRSCHEMA".input_queue_template
(
   value_time timestamp without time zone NOT NULL,
   update_time timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
   tick_volume double precision NOT NULL DEFAULT 1,
   PRIMARY KEY (value_time)
)
WITH (
  OIDS = FALSE
)
;
ALTER TABLE :"SVRSCHEMA".input_queue_template
  OWNER TO :SVRUSER;


CREATE TABLE :"SVRSCHEMA".decon_queue_template
(
   value_time timestamp without time zone NOT NULL,
   update_time timestamp without time zone NOT NULL DEFAULT NOW(),
   tick_volume double precision NOT NULL DEFAULT 1.0,
   PRIMARY KEY (value_time)
)
WITH (
  OIDS = FALSE
);
ALTER TABLE :"SVRSCHEMA".decon_queue_template
  OWNER TO :SVRUSER;


CREATE TABLE :"SVRSCHEMA".decon_queues
(
  table_name text PRIMARY KEY,
  input_queue_table_name text NOT NULL REFERENCES :"SVRSCHEMA".input_queues ON UPDATE CASCADE ON DELETE NO ACTION,
  input_queue_column_name text NOT NULL,
  dataset_id bigint NOT NULL,
  CONSTRAINT unique_decon_per_dataset_and_inpu_queue UNIQUE (input_queue_table_name, input_queue_column_name, dataset_id)
)
WITH (
  OIDS = FALSE
);
ALTER TABLE :"SVRSCHEMA".decon_queues
  OWNER TO :SVRUSER;


CREATE TABLE :"SVRSCHEMA".datasets
(
  id bigint PRIMARY KEY,
  dataset_name text NOT NULL,
  user_name text NOT NULL,
  main_input_queue_table_name text NOT NULL REFERENCES :"SVRSCHEMA".input_queues ON UPDATE CASCADE ON DELETE NO ACTION,
  aux_input_queues_table_names text[],
  priority smallint NOT NULL,
  description text,
  swt_levels smallint NOT NULL,
  swt_wavelet_name text NOT NULL,
  max_gap interval NOT NULL,
  is_active boolean DEFAULT false
)
WITH (
  OIDS = FALSE
);
ALTER TABLE :"SVRSCHEMA".datasets
  OWNER TO :SVRUSER;

CREATE TABLE :"SVRSCHEMA".ensembles
(
  id bigint PRIMARY KEY,
  dataset_id bigint NOT NULL REFERENCES :"SVRSCHEMA".datasets ON UPDATE CASCADE ON DELETE NO ACTION,  
  decon_queue_table_name text REFERENCES :"SVRSCHEMA".decon_queues ON UPDATE CASCADE ON DELETE NO ACTION,
  aux_decon_queues_table_names text[] --ELEMENT REFERENCES :"SVRSCHEMA".decon_queues ON UPDATE CASCADE ON DELETE NO ACTION
)
WITH (
  OIDS = FALSE
);
ALTER TABLE :"SVRSCHEMA".ensembles
  OWNER TO :SVRUSER;

CREATE TABLE :"SVRSCHEMA".svr_parameters
(
  id bigint PRIMARY KEY,
  dataset_id bigint NOT NULL REFERENCES :"SVRSCHEMA".datasets ON UPDATE CASCADE ON DELETE NO ACTION,
  input_queue_table_name text NOT NULL REFERENCES :"SVRSCHEMA".input_queues ON UPDATE CASCADE ON DELETE NO ACTION,
  input_queue_column_name text NOT NULL,
  decon_level smallint NOT NULL,
  svr_c double precision NOT NULL,
  svr_epsilon double precision NOT NULL,
  svr_kernel_param double precision NOT NULL,
  svr_kernel_param2 double precision NOT NULL,
  svr_decremental_distance bigint NOT NULL,
  svr_adjacent_levels_ratio double precision NOT NULL,
  svr_kernel_type smallint NOT NULL,
  lag_count smallint NOT NULL,
  CONSTRAINT unique_svr_parameters UNIQUE (dataset_id, input_queue_table_name, input_queue_column_name, decon_level)
)
WITH (
  OIDS = FALSE
);
ALTER TABLE :"SVRSCHEMA".svr_parameters
  OWNER TO :SVRUSER;

CREATE TABLE :"SVRSCHEMA".models
(
  id bigint PRIMARY KEY,
  ensemble_id bigint NOT NULL REFERENCES :"SVRSCHEMA".ensembles ON UPDATE CASCADE ON DELETE NO ACTION,
  decon_level smallint NOT NULL,
  learning_levels smallint[],
  model_binary bytea,
  last_modified_time timestamp without time zone,
  last_modeled_value_time timestamp without time zone,
  norm_mean_coef double precision,
  norm_range_coef double precision
  )
WITH (
  OIDS = FALSE
);
ALTER TABLE :"SVRSCHEMA".models
  OWNER TO :SVRUSER;


CREATE TABLE :"SVRSCHEMA".prediction_tasks
(
  id bigint PRIMARY KEY,
  dataset_id bigint NOT NULL REFERENCES :"SVRSCHEMA".datasets ON UPDATE CASCADE ON DELETE NO ACTION,
  start_time timestamp without time zone NOT NULL,
  end_time timestamp without time zone NOT NULL,
  start_prediction_time timestamp without time zone NOT NULL,
  end_prediction_time timestamp without time zone NOT NULL,
  status smallint default 0,
  mse double precision default -1.0
)
WITH (
  OIDS = FALSE
);
ALTER TABLE :"SVRSCHEMA".prediction_tasks
  OWNER TO :SVRUSER;

CREATE TABLE :"SVRSCHEMA".autotune_tasks
(
  id bigint NOT NULL,
  dataset_id bigint NOT NULL REFERENCES :"SVRSCHEMA".datasets ON UPDATE CASCADE ON DELETE NO ACTION,
  result_dataset_id bigint,
  creation_time timestamp without time zone NOT NULL DEFAULT now(),
  done_time timestamp without time zone,
  parameters json NOT NULL,
  start_train_time timestamp without time zone NOT NULL,
  end_train_time timestamp without time zone NOT NULL,
  start_tuning_time timestamp without time zone NOT NULL,
  end_tuning_time timestamp without time zone NOT NULL,
  vp_sliding_direction smallint,
  vp_slide_count smallint,
  vp_slide_period_sec bigint,
  pso_best_points_counter smallint,
  pso_iteration_number smallint,
  pso_particles_number smallint,
  pso_topology smallint,
  nm_max_iteration_number smallint,
  nm_tolerance double precision,
  status smallint DEFAULT 0,
  mse double precision DEFAULT -1.0,
  PRIMARY KEY (id)
)
WITH (
  OIDS = FALSE
);
ALTER TABLE :"SVRSCHEMA".autotune_tasks
  OWNER TO :SVRUSER;

CREATE TABLE parmtune_decrement_tasks
(
  id bigint NOT NULL, -- Task id number.
  dataset_id bigint NOT NULL,
  start_task_time timestamp without time zone NOT NULL,
  end_task_time timestamp without time zone, -- When time is set, task has ended.
  start_train_time timestamp without time zone NOT NULL, -- Start of train range.
  end_train_time timestamp without time zone NOT NULL, -- End of train range.
  start_validation_time timestamp without time zone NOT NULL, -- Start of training range.
  end_validation_time timestamp without time zone NOT NULL, -- End of training range.
  parameters json NOT NULL, -- Add sliding window options here
  status smallint, -- Task status and resultant error code if done.
  decrement_step text NOT NULL,
  vp_sliding_direction smallint,
  vp_slide_count smallint,
  vp_slide_period_sec bigint,
  "values" json, -- Contains a vector of MSE vs observations count, and the recommended observation count for every level of the dataset predicted input queue.
  suggested_value json,
  CONSTRAINT pk_parmtune_decrement_tasks_id PRIMARY KEY (id),
  CONSTRAINT fk_parmtune_decrement_dataset_id FOREIGN KEY (dataset_id)
      REFERENCES datasets (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE parmtune_decrement_tasks
  OWNER TO :SVRUSER;
COMMENT ON COLUMN parmtune_decrement_tasks.id IS 'Task id number.';
COMMENT ON COLUMN parmtune_decrement_tasks.end_task_time IS 'When time is set, task has ended.';
COMMENT ON COLUMN parmtune_decrement_tasks.start_train_time IS 'Start of training range.';
COMMENT ON COLUMN parmtune_decrement_tasks.end_train_time IS 'End of training range.';
COMMENT ON COLUMN parmtune_decrement_tasks.status IS 'Task status and resultant error code if done.';
COMMENT ON COLUMN parmtune_decrement_tasks."values" IS 'Contains a vector of MSE vs observations count, and the recommended observation count for every level of the dataset predicted input queue.';

CREATE TABLE :"SVRSCHEMA".scaling_factors
(
  id bigint PRIMARY KEY,
  dataset_id bigint NOT NULL REFERENCES :"SVRSCHEMA".datasets ON UPDATE CASCADE ON DELETE NO ACTION,
  input_queue_table_name text NOT NULL,
  input_queue_column_name text NOT NULL,
  wavelet_level smallint NOT NULL,
  scaling_factor double precision default 1.0
)
WITH (
  OIDS = FALSE
);
ALTER TABLE :"SVRSCHEMA".scaling_factors
  OWNER TO :SVRUSER;



CREATE TABLE :"SVRSCHEMA".user_datasets
(
  user_id bigint NOT NULL REFERENCES :"SVRSCHEMA".userauth ON UPDATE CASCADE ON DELETE CASCADE,
  dataset_id bigint NOT NULL REFERENCES :"SVRSCHEMA".datasets ON UPDATE CASCADE ON DELETE CASCADE,
  PRIMARY KEY (user_id, dataset_id)
)
WITH (
  OIDS = FALSE
);
ALTER TABLE :"SVRSCHEMA".user_datasets
  OWNER TO :SVRUSER;

CREATE OR REPLACE VIEW :"SVRSCHEMA".v_user_datasets AS 
 SELECT DISTINCT all_ds.id,
    all_ds.dataset_name,
    all_ds.user_name,
    all_ds.main_input_queue_table_name,
    all_ds.aux_input_queues_table_names,
    all_ds.priority,
    all_ds.description,
    all_ds.swt_levels,
    all_ds.swt_wavelet_name,
    all_ds.max_gap,
    all_ds.is_active,
    all_ds.linked_user_name,
    all_ds.user_priority
   FROM (( SELECT ds.id,
            ds.dataset_name,
            ds.user_name,
            ds.main_input_queue_table_name,
            ds.aux_input_queues_table_names,
            ds.priority,
            ds.description,
            ds.swt_levels,
            ds.swt_wavelet_name,
            ds.max_gap,
            ds.is_active,
            ua.username as linked_user_name,
            ua.priority as user_priority
           FROM datasets ds
             JOIN user_datasets ud ON ds.id = ud.dataset_id
             JOIN userauth ua ON ua.user_id = ud.user_id
          WHERE ds.is_active = true)
        UNION ALL
        ( SELECT ds.id,
            ds.dataset_name,
            ds.user_name,
            ds.main_input_queue_table_name,
            ds.aux_input_queues_table_names,
            ds.priority,
            ds.description,
            ds.swt_levels,
            ds.swt_wavelet_name,
            ds.max_gap,
            ds.is_active,
            ua.username as linked_user_name,
            ua.priority as user_priority
           FROM datasets ds
             JOIN userauth ua ON ua.username = ds.user_name
          WHERE ds.is_active = true)) all_ds
ORDER BY all_ds.user_priority DESC, all_ds.priority DESC;

ALTER TABLE :"SVRSCHEMA".v_user_datasets
  OWNER TO :SVRUSER;

-- ******************************
-- CREATE SVRWAVE SEQUENCES     *
-- ******************************

CREATE SEQUENCE :"SVRSCHEMA".userauth_seq
   INCREMENT 1
   START 100;
ALTER SEQUENCE :"SVRSCHEMA".userauth_seq
  OWNER TO :SVRUSER;

CREATE SEQUENCE :"SVRSCHEMA".datasets_seq
   INCREMENT 1
   START 100;
ALTER SEQUENCE :"SVRSCHEMA".datasets_seq
  OWNER TO :SVRUSER;

CREATE SEQUENCE :"SVRSCHEMA".svr_parameters_seq
   INCREMENT 1
   START 100;
ALTER SEQUENCE :"SVRSCHEMA".svr_parameters_seq
  OWNER TO :SVRUSER;

CREATE SEQUENCE :"SVRSCHEMA".models_seq
   INCREMENT 1
   START 100;
ALTER SEQUENCE :"SVRSCHEMA".models_seq
  OWNER TO :SVRUSER;

CREATE SEQUENCE :"SVRSCHEMA".ensembles_seq
    INCREMENT 1
    START 100;
ALTER SEQUENCE :"SVRSCHEMA".ensembles_seq
 OWNER TO :SVRUSER;

CREATE SEQUENCE :"SVRSCHEMA".request_id_seq
  INCREMENT 1
  START 100;
ALTER SEQUENCE :"SVRSCHEMA".request_id_seq
  OWNER TO :SVRUSER;

CREATE SEQUENCE :"SVRSCHEMA".prediction_tasks_seq
  INCREMENT 1
  START 100;
ALTER SEQUENCE :"SVRSCHEMA".prediction_tasks_seq
  OWNER TO :SVRUSER;

CREATE SEQUENCE :"SVRSCHEMA".autotune_tasks_seq
  INCREMENT 1
  START 100;
ALTER SEQUENCE :"SVRSCHEMA".autotune_tasks_seq
  OWNER TO :SVRUSER;

CREATE SEQUENCE :"SVRSCHEMA".parmtune_decrement_tasks_seq
  INCREMENT 1
  START 100;
ALTER SEQUENCE :"SVRSCHEMA".parmtune_decrement_tasks_seq
  OWNER TO :SVRUSER;

CREATE SEQUENCE :"SVRSCHEMA".scaling_factors_seq
  INCREMENT 1
  START 100;
ALTER SEQUENCE :"SVRSCHEMA".scaling_factors_seq
  OWNER TO :SVRUSER;

-- ******************************
-- STORED PROCEDURES            *
-- ******************************

CREATE OR REPLACE FUNCTION public.cleanup_queue(
        tbl regclass,
            value_times timestamp without time zone[])
          RETURNS void AS
        $BODY$
        BEGIN
                EXECUTE format('delete from %s q1 where exists (select 1 from unnest(%L::timestamp[]) tm where tm = q1.value_time)', tbl, value_times);
            END
            $BODY$
              LANGUAGE plpgsql VOLATILE
              COST 100;
            ALTER FUNCTION public.cleanup_queue(regclass, timestamp without time zone[])
              OWNER TO :SVRUSER;


REVOKE ALL ON SCHEMA :"SVRSCHEMA" FROM PUBLIC;
REVOKE ALL ON SCHEMA :"SVRSCHEMA" FROM :SVRUSER;
GRANT ALL ON SCHEMA :"SVRSCHEMA" TO :SVRUSER;
GRANT ALL ON SCHEMA :"SVRSCHEMA" TO PUBLIC;

--
-- PostgreSQL database dump complete
--
